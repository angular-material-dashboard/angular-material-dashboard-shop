# AMD Shop 

[![pipeline status](https://gitlab.com/angular-material-dashboard/angular-material-dashboard-shop/badges/master/pipeline.svg)](https://gitlab.com/angular-material-dashboard/angular-material-dashboard-shop/commits/master)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/0503fd7e79dd4031bc3c661ef1b7e9fe)](https://www.codacy.com/app/angular-material-dashboard/angular-material-dashboard-shop?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=angular-material-dashboard/angular-material-dashboard-shop&amp;utm_campaign=Badge_Grade)

A module to add management on products or services to angular-material-dashboard. 

## Install

This is a bower module and you can install it as follow:

	bower install --save angular-material-dashboard-shop

It is better to address the repository directly

	bower install --save https://gitlab.com/angular-material-dashboard/angular-material-dashboard-shop.git

## Document

- [Reference document](https://angular-material-dashboard.gitlab.io/angular-material-dashboard-shop/doc/index.html)