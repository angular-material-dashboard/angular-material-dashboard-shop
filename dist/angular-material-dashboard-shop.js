/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardShop', [
    'ngMaterialDashboard',
    'seen-shop'
]);

'use strict';

angular.module('ngMaterialDashboardShop')
/*
 * ماشین حالت نرم افزار
 */
.config(function ($routeProvider) {

    $routeProvider//

    /*
     * Categories
     */
    .when('/shop/categories', {
        templateUrl : 'views/amd-shop-categories.html',
        name : 'Categories',
        icon : 'folder_special',
        groups : [ 'shop-management' ],
        navigate : true,
        /*
         * @ngInject
         */
        protect : function ($rootScope) {
            return !$rootScope.app.user.tenant_owner;
        },
    })//
    
    .when('/shop/categories/:categoryId', {
        templateUrl : 'views/amd-shop-category.html',
        navigate : false,
        /*
         * @ngInject
         */
        protect : function ($rootScope) {
            return !$rootScope.app.user.tenant_owner;
        },
    })//
    
    /*
     * Product
     */
    .when('/shop/products', {
        templateUrl : 'views/amd-shop-products.html',
        name : 'Products',
        icon : 'add_shopping_cart',
        groups : [ 'shop-management' ],
        navigate : true,
        /*
         * @ngInject
         */
        protect : function ($rootScope) {
            return !$rootScope.app.user.tenant_owner;
        },
    })//
    .when('/shop/products/:productId', {
        templateUrl : 'views/amd-shop-product.html',
        navigate : false,
        /*
         * @ngInject
         */
        protect : function ($rootScope) {
            return !$rootScope.app.user.tenant_owner;
        },
    })//
    /*
     * Service
     */
    .when('/shop/services', {
        templateUrl : 'views/amd-shop-services.html',
        name : 'Services',
        icon : 'cloud_upload',
        groups : [ 'shop-management' ],
        navigate : true,
        /*
         * @ngInject
         */
        protect : function ($rootScope) {
            return !$rootScope.app.user.tenant_owner;
        },
    })//
    .when('/shop/services/:serviceId', {
        templateUrl : 'views/amd-shop-service.html',
        navigate : false,
        /*
         * @ngInject
         */
        protect : function ($rootScope) {
            return !$rootScope.app.user.tenant_owner;
        },
    })//
    
    /*
     * Tags
     */
    .when('/shop/tags', {
        templateUrl : 'views/amd-shop-tags.html',
        name : 'Tags',
        icon : 'label',
        groups : [ 'shop-management' ],
        navigate : true,
        /*
         * @ngInject
         */
        protect : function ($rootScope) {
            return !$rootScope.app.user.tenant_owner;
        },
    })//
    .when('/shop/tags/:tagId', {
        templateUrl : 'views/amd-shop-tag.html',
        navigate : false,
        /*
         * @ngInject
         */
        protect : function ($rootScope) {
            return !$rootScope.app.user.tenant_owner;
        },
    })
    
    /*
     * Orders
     */
    .when('/shop/orders', {
        templateUrl : 'views/amd-shop-orders.html',
        name : 'Orders',
        icon : 'event',
        groups : [ 'shop-management' ],
        navigate : true,
        /*
         * @ngInject
         */
        protect : function ($rootScope) {
            return !$rootScope.app.user.tenant_owner;
        },
    })//
    .when('/shop/orders/:orderId', {
        controller : 'AmdShopOrderCtrl',
        controllerAs : 'ctrl',
        templateUrl : 'views/amd-shop-order.html',
        navigate : false,
        /*
         * @ngInject
         */
        protect : function ($rootScope) {
            return !$rootScope.app.user.tenant_owner;
        },
    })//
    
    /*
     * Deliveries
     */
    .when('/shop/delivers', {
        templateUrl : 'views/amd-shop-delivers.html',
        name : 'Delivers',
        icon : 'local_shipping',
        groups : [ 'shop-management' ],
        navigate : true,
        /*
         * @ngInject
         */
        protect : function ($rootScope) {
            return !$rootScope.app.user.tenant_owner;
        },
    })//
    .when('/shop/delivers/:deliverId', {
        controller : 'AmdShopDeliverCtrl',
        templateUrl : 'views/amd-shop-deliver.html',
        navigate : false,
        /*
         * @ngInject
         */
        protect : function ($rootScope) {
            return !$rootScope.app.user.tenant_owner;
        },
    });




    /************************************************************************
     * Zone
     ***********************************************************************/

    /*
     * @ngInject
     */
    function canAccessZones($rootScope) {
        return !$rootScope.app.user.tenant_owner;
    }

    $routeProvider.when('/shop/zones', {
        templateUrl : 'views/amd-shop-zones.html',
        name : 'Zones',
        icon : 'layers',
        groups : [ 'shop-management' ],
        navigate : true,
        /*
         * @ngInject
         */
        protect : canAccessZones,
    })//
    .when('/shop/zones/:zoneId', {
        templateUrl : 'views/amd-shop-zone.html',
        navigate : false,
        /*
         * @ngInject
         */
        protect : canAccessZones,
    });


    /************************************************************************
     * Agency
     ***********************************************************************/
    /*
     * @ngInject
     */
    function canAccessAgency($rootScope) {
        return !$rootScope.app.user.tenant_owner;
    }

    $routeProvider.when('/shop/agencies', {
        templateUrl : 'views/amd-shop-agencies.html',
        name : 'Agencies',
        icon : 'store',
        groups : [ 'shop-management' ],
        navigate : true,
        protect : canAccessAgency,
    })//
    .when('/shop/agencies/:itemId', {
        templateUrl : 'views/amd-shop-agency.html',
        navigate : false,
        protect : canAccessAgency,
    });
});
/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

angular.module('ngMaterialDashboardShop')

/**
 * @ngdoc Controller
 * @name AmdShopAgencyCtrl
 * @description Manages an agency from shop domain
 */
.controller('AmdShopAgencyCtrl', function(
    /* angularjs  */ $scope, $controller, $element,
    /* ngRoute    */ $routeParams,
    /* seen-shp   */ $shop,
    /* mblowfish  */ $actions) {

    angular.extend(this, $controller('MbSeenAbstractItemCtrl', {
        $scope : $scope,
        $element: $element
    }));

    // delete model
	this.deleteModel = function(item){
		return $shop.deleteAgency(item.id);
	};

	// get model schema
	this.getModelSchema = function(){
		return $shop.agencySchema();
	};

	// get model
	this.getModel = function(id){
		return $shop.getAgency(id);
	};

	// update model
	this.updateModel = function(item){
		return item.update();
	};


    this.init({
        eventType: '/shop/agencies'
    });
});




angular.module('ngMaterialDashboardShop')

/**
 * @ngdoc Controller
 * @name AmdShopCategoryCtrl
 */
.controller('AmdShopCategoryCtrl', function ($scope, $shop,
        $routeParams, $location, $translate, QueryParameter, $navigator) {

    var ctrl = {
            loading: false,
            saving: false,
            savingCategory: false,
            loadingSubcategories: false,
            loadingServices: false,
            loadingProducts: false,
            edit: false,
            items: [],
            subcategories: [],
            services: [],
            products: [],
            serviceSaving: false,
            addingService: false,
            productSaving: false,
            addingProduct: false,
            parentLoading: false
    };


    function loadCategory() {
        if (ctrl.loading) {
            return;
        }
        ctrl.loading = true;
        $shop.getCategory($routeParams.categoryId)//
        .then(function (cat) {
            $scope.category = cat;
            loadParentCategory(cat.parent_id);
            loadSubcategories();
            loadServices();
            loadProducts();
        }, function () {
            alert($translate.instant('Failed to load the category.'));
        })//
        .finally(function () {
            ctrl.loading = false;
        });
    }

    function remove() {
        confirm($translate.instant('Item will be deleted. There is no undo action.'))//
        .then(function () {
            return $scope.category.delete();//
        })//
        .then(function () {
            $location.path('/categories');
        }, function () {
            alert($translate.instant('Fail to delete the category.'));
        });
    }

    function save() {
        if (ctrl.saving) {
            return;
        }
        ctrl.saving = true;
        $scope.category.update()//
        .then(function (newCat) {
            $scope.category = newCat;
            ctrl.edit = false;
        }, function () {
            alert($translate.instant('Failed to save the category.'));
        })//
        .finally(function () {
            ctrl.saving = false;
        });
    }

    function loadSubcategories() {
        if (ctrl.loadingSubcategories) {
            return;
        }
        ctrl.loadingSubcategories = true;
        var pp = new QueryParameter();
        pp.setFilter('parent_id', $scope.category.id);
        $shop.getCategories(pp)//
        .then(function (clist) {
            ctrl.subcategories = clist.items;
        }, function () {
            alert($translate.instant('Failed to load subcategories.'));
        })//
        .finally(function () {
            ctrl.loadingSubcategories = false;
        });
    }

    function loadServices() {
        if (ctrl.loadingServices) {
            return;
        }
        ctrl.loadingServices = true;
        $scope.category.getServices()//
        .then(function (slist) {
            ctrl.services = slist.items;
        }, function () {
            alert($translate.instant('Failed to load services.'));
        })//
        .finally(function () {
            ctrl.loadingServices = false;
        });
    }

    function loadProducts() {
        if (ctrl.loadingProducts) {
            return;
        }
        ctrl.loadingProducts = true;
        $scope.category.getProducts()//
        .then(function (plist) {
            ctrl.products = plist.items;
        }, function () {
            alert($translate.instant('Failed to load products.'));
        })//
        .finally(function () {
            ctrl.loadingProducts = false;
        });
    }


    /**
     * @param {type} id
     * @returns {undefined}
     */
    function loadParentCategory(id) {
        if (ctrl.parentLoading) {
            return;
        }
        if (!id) {
            $scope.parent = {
                    name: 'No parent (Root)'
            };
            return;
        }
        ctrl.parentLoading = true;
        $shop.getCategory(id)//
        .then(function (parent) {
            $scope.parent = parent;
        }, function () {
            alert($translate.instant('Failed to load parent category.'));
        })//
        .finally(function () {
            ctrl.parentLoading = false;
        });
    }

    /**
     * Creates new category
     */
    function addNewCategory() {
        if (ctrl.savingCategory) {
            return;
        }
        return ctrl.savingCategory = $navigator.openDialog({
            templateUrl: 'views/dialogs/category-new.html',
            config: {}
        })
        .then(function (newConfig) {
            newConfig.parent_id = $scope.category.id;
            return $shop.putCategory(newConfig);
        })
        .then(function (cat) {
            ctrl.subcategories = ctrl.subcategories.concat(cat);
        }, function () {
            alert($translate.instant('Fail to create new category.'));
        })//
        .finally(function () {
            ctrl.savingCategory = false;
        });
    }

    /*
     * SERVICE SECTION
     * 
     * Get service properties using dialog
     */

    function addNewService() {
        return $navigator.openDialog({
            templateUrl: 'views/dialogs/service-new.html',
            config: {}
        }).then(function (newConfig) {
            createService(newConfig);
        });
    }

    /*
     * Create service 
     */
    function createService(service) {
        if (ctrl.serviceSaving) {
            return;
        }
        ctrl.serviceSaving = true;
        return $shop.putService(service)
        .then(function (service) {
            return service;
        }, function () {
            alert($translate.instant('Fail to create new service.'));
        })//
        .then(function (service) {
            addServiceToCategory(service);
        })//
        .finally(function () {
            ctrl.serviceSaving = false;
        });
    }

    /*
     * Add service to the services of current category
     */
    function addServiceToCategory(service) {
        if (ctrl.addingService) {
            return;
        }
        ctrl.addingService = true;
        service.itemId = service.id;
        $scope.category.putService(service)//
        .then(function (servic) {
            ctrl.services = ctrl.services.concat(servic);
        }, function () {
            alert($translate.instant('Fail to add service to the category.'));
        })//
        .finally(function () {
            ctrl.addingService = false;
        });
    }


    /*
     * PRODUCT SECTION
     * 
     * Get product properties using dialog
     */
    function addNewProduct() {
        return $navigator.openDialog({
            templateUrl: 'views/dialogs/product-new.html',
            config: {}
        })//
        .then(function (newConfig) {
            createProduct(newConfig);
        });
    }

    /*
     * Create product
     */
    function createProduct(product) {
        if (ctrl.productSaving) {
            return;
        }
        ctrl.productSaving = true;
        return $shop.putProduct(product)
        .then(function (product) {
            return product;
        }, function () {
            alert($translate.instant('Fail to create new product.'));
        })
        .then(function (product) {
            addProductToCategory(product);
        })//
        .finally(function () {
            ctrl.productSaving = false;
        });
    }

    /*
     * Add product to the products of current category
     */
    function addProductToCategory(product) {
        if (ctrl.addingProduct) {
            return;
        }
        ctrl.addingProduct = true;
        product.itemId = product.id;
        $scope.category.putProduct(product)//
        .then(function (prod) {
            ctrl.products = ctrl.products.concat(prod);
        }, function () {
            alert($translate.instant('Fail to add product to the category.'));
        })//
        .finally(function () {
            ctrl.addingProduct = false;
        });
    }

    // Actions
    $scope.categoryActions = [{
        title: 'New category',
        icon: 'add',
        action: addNewCategory
    }];

    $scope.serviceActions = [{
        title: 'New service',
        icon: 'add',
        action: addNewService
    }];

    $scope.productActions = [{
        title: 'New product',
        icon: 'add',
        action: addNewProduct
    }];

    $scope.category = {};
    $scope.remove = remove;
    $scope.save = save;
    $scope.ctrl = ctrl;
    loadCategory();

});


'use strict';

angular.module('ngMaterialDashboardShop')

/**
 * @ngdoc Controller
 * @name AmdShopDeliveryCtrl
 * @description Controller of a Delivery
 */
.controller('AmdShopDeliverCtrl', function ($scope, $shop, $routeParams, $translate, $location, $navigator, $q, QueryParameter) {

    var ctrl = {
            loading: false,
            updating: false,
            edit: false
    };

    /**
     * @name loadDelivery
     * @memberOf AmdShopDeliveryCtrl
     * @description Load the selected deliver
     */
    function loadDeliver() {
        if (ctrl.loading) {
            return;
        }
        ctrl.loading = true;
        $shop.getDeliver($routeParams.deliverId)//
        .then(function (p) {
            $scope.deliver = p;
        }, function () {
            alert($translate.instant('Faild to load the deliver.'));
        })//
        .finally(function () {
            ctrl.loading = false;
        });
    }

    /**
     * @name remove
     * @memberOf AmdShopDeliveryCtrl
     * @description remove the selected deliver from the server
     */
    function remove() {
        confirm($translate.instant('Item will be deleted. There is no undo action.'))//
        .then(function () {
            return $scope.deliver.delete()//
            .then(function () {
                $location.path('/delivers/');
            }, function () {
                alert('Fail to delete the deliver.');
            });
        });
    }

    /**
     * @name save
     * @memberOf AmdShopDeliveryCtrl
     * @description Update the selected deliver
     */
    function save() {
        if (ctrl.updating) {
            return;
        }
        ctrl.updating = true;
        $scope.deliver.update()//
        .then(function (newDeliver) {
            $scope.deliver = newDeliver;
            ctrl.edit = false;
        }, function () {
            alert($translate.instant('Failed to update deliver.'));
        })//
        .finally(function () {
            ctrl.updating = false;
        });
    }

    /*
     * تمام امکاناتی که در لایه نمایش ارائه می‌شود در اینجا نام گذاری شده است.
     */
    $scope.remove = remove;
    $scope.save = save;
    $scope.ctrl = ctrl;

    loadDeliver();
});


'use strict';

angular.module('ngMaterialDashboardShop')

/**
 * @ngdoc Controller
 * @name AmdShopOrderCtrl
 * @description load the order
 */
.controller('AmdShopOrderCtrl', function (ShopOrder, $shop, $translate, $routeParams, $navigator, $window) {

    this.order = {};
    this.actions = [];
    this.totalP = 0;

    this.loadOrder = function () {
        if (this.loading) {
            return;
        }
        this.loading = true;
        var ctrl = this;
        $shop.getOrder($routeParams.orderId, {
            graphql: '{id,title,full_name,phone,email,province,city,address,description,state,creation_dtime,modif_dtime,order_items{title,price,count,item_type},histories{action,description},customer{id,profiles{first_name,last_name}}, attachments{id,description,mime_type,file_name,file_size},assignee{id,profiles{first_name,last_name}},metafields{key,value,namespace}}'
        })//
        //graphql: '{id,title,full_name,phone,email,address,description,state,order_items{title,price,count,item_type},histories{action,description},customer{id,profiles{first_name,last_name}},assignee{id,profiles{first_name,last_name}}}'
        .then(function (order) {
            ctrl.initialize(order);
        }, function () {
            alert($translate.instant('Failed to load the order.'));
        })//
        .finally(function () {
            ctrl.loading = false;
        });
    };

    this.initialize = function (order) {
        this.order = new ShopOrder(order);
        this.loadActions();
        this.totalPrice();
    };

    /**
     * Calculate total price of the order.
     * 
     * @memberof AmdShopOrderCtrl
     */
    this.totalPrice = function () {
        var totalPrice = 0;
        angular.forEach(this.order.order_items, function(orderItm){
            totalPrice += orderItm.price * orderItm.count;
        });
        this.totalP = totalPrice;
    };

    this.loadActions = function () {
        if (this.actionLoading) {
            return;
        }
        this.actionLoading = true;
        var ctrl = this;
        return this.order.getPossibleTransitions()
        .then(function (res) {
            ctrl.actions = res.items;
        }, function () {
            alert($translate.instant('Failed to load possible actions.'));
        })//
        .finally(function () {
            ctrl.actionLoading = false;
        });
    };

    this.doAction = function (transition) {
        if (this.actionDoing) {
            return;
        }
        this.actionDoing = true;
        var promise;
        var action = {
                action: transition.id
        };
        var ctrl = this;
        if (!transition.properties) {
            promise = this.order.putTransition(action);
        } else {
            promise = $navigator.openDialog({
                templateUrl: 'views/dialogs/action-properties.html',
                config: {
                    action: action,
                    properties: transition.properties
                }
            })
            .then(function (action) {
                return ctrl.order.putTransition(action);
            });
        }
        return promise//
        //TODO: handle the response
        .finally(function () {
            ctrl.actionDoing = false;
            ctrl.loadOrder();
        });
    };

    this.showImage = function (order, attachment) {
        $navigator.openDialog({
            templateUrl: 'views/dialogs/show-image.html',
            config: {
                order: order,
                attachment: attachment
            }
        });
    };

    this.callToCustomer = function () {
        var str = 'tel:' + this.order.phone;
        $window.open(str);
    };

    this.loadOrder();
});


'use strict';

angular.module('ngMaterialDashboardShop')

/**
 * @ngdoc Controller
 * @name AmdShopProductCtrl
 * @description Controller of products list
 */
.controller('AmdShopProductCtrl', function ($scope, $shop, $routeParams, $translate, $navigator, $location, $q, QueryParameter ) {

    var ctrl = {
            loading: false,
            updating: false,
            edit: false,
            loadingMetas: false,
            loadingCategories: false
    };

    /**
     * @name loadProduct
     * @memberOf AmdShopProductCtrl
     * @description Load the selected product
     */
    function loadProduct() {
        if (ctrl.loading) {
            return;
        }
        ctrl.loading = true;
        $shop.getProduct($routeParams.productId)//
        .then(function (p) {
            $scope.product = p;
            loadMetas();
            loadCategories();
        }, function () {
            alert($translate.instant('Faild to load the product.'));
        })//
        .finally(function () {
            ctrl.loading = false;
        });
    }

    /**
     * @name remove
     * @memberOf AmdShopProductCtrl
     * @description Remove the selected product from the server
     */
    function remove() {
        confirm($translate.instant('Item will be deleted. There is no undo action.'))//
        .then(function () {
            return $scope.product.delete()//
            .then(function () {
                $location.path('/products');
            }, function () {
                alert($translate.instant('Fail to delete the product.'));
            });
        });
    }

    /**
     * @name update
     * @memberOf AmdShopProductCtrl
     * @description Update the selected product
     */
    function update() {
        if (ctrl.updating) {
            return;
        }
        ctrl.updating = true;
        $scope.product.update()//
        .then(function (newProduct) {
            $scope.product = newProduct;
            ctrl.edit = false;
        }, function () {
            alert($translate.instant('Failed to update product.'));
        })//
        .finally(function () {
            ctrl.updating = false;
        });
    }

    /**
     * @name loadMetas
     * @memberOf AmdShopProductCtrl
     * @description Load the metadatas of the product
     */
    function loadMetas() {
        if (ctrl.loadingMetas) {
            return;
        }
        ctrl.loadingMetas = true;
        $scope.product.getMetafields()
        .then(function (metaFields) {
            $scope.metafeilds = metaFields.items;
        }, function () {
            alert($translate.instant('Faild to get metafields.'));
        })//
        .finally(function () {
            ctrl.loadingMetas = false;
        });
    }

    /*
     * Load categories the product belongs to.
     */
    function loadCategories() {
        if (ctrl.loadingCategories) {
            return;
        }
        ctrl.loadingCategories = true;
        $scope.product.getCategories()
        .then(function (res) {
            $scope.categories = res.items;
        }, function () {
            alert($translate.instant('Faild to get categories.'));
        })//
        .finally(function () {
            ctrl.loadingCategories = false;
        });
    }

    /**
     * @name removeMetaField
     * @memberOf AmdShopProductCtrl
     * @description Remove a metadata from the metadatas of the product
     * @param {type}
     *            metaData
     */
    function removeMetafield(metaData) {
        confirm($translate.instant('Item will be deleted. There is no undo action.'))//
        .then(function () {
            return $scope.product.deleteMetafield(metaData)//
            .then(function () {
                loadMetas();
                toast($translate.instant('Item is deleted successfully.'));
            }, function () {
                alert($translate.instant('Failed to delete item.'));
            });
        });
    }

    function addMetafield(metadata) {
        var mydata = metadata ? metadata : {};
        $navigator.openDialog({
            templateUrl: 'views/dialogs/metafield-new.html',
            config: {
                data: mydata
            }
        // Create content
        }).then(function (meta) {
            return $scope.product.putMetafield(meta)//
            .then(function () {
                loadMetas();
            }, function () {
                alert($translate.instant('Failed to add new item.'));
            });
        });
    }

    function updateMetafield(metadata) {
        $navigator.openDialog({
            templateUrl: 'views/dialogs/metafield-update.html',
            config: {
                data: metadata
            }
        // Create content
        }).then(function (meta) {
            return $scope.product.putMetafield(meta)//
            .then(function () {
                loadMetas();
            }, function () {
                alert($translate.instant('Failed to update item.'));
            });
        });
    }

    function inlineUpdateMetafield(metadata) {
        return $scope.product.putMetafield(metadata)//
        .then(function () {
            loadMetas();
        }, function () {
            alert($translate.instant('Failed to update item.'));
        });
    }

    /*
     * Assign some categories to the product.
     */
    function selectCategories() {
        loadAllCategories()
        .then(function (allCategories) {
            return cleanCategories(allCategories);
        })
        .then(function (allCategories) {
            return $navigator.openDialog({
                templateUrl: 'views/dialogs/select-categories.html',
                config: {
                    data: allCategories
                }
            });
        })
        .then(function (newCats) {
            updateCategories(newCats);
        });
    }

    var categories = [];
    /*
     * @returns {.$q@call;defer.promise}
     * @description Load all categories defines in the shop
     */
    function loadAllCategories() {
        var pp = new QueryParameter();
        pp.setOrder('name', 'a');
        return $shop.getCategories(pp)//
        .then(function (res) {
            categories = res.items;
            return categories;
        });
    }

    /*
     * @param {type} allCategories
     * @returns {allCategories}
     * @description Add 'selected' field to each category in categories and assign it true or false
     */
    function cleanCategories(allCategories) {
        var deferred = $q.defer();
        for (var i = 0; i < allCategories.length; i++) {
            allCategories[i].selected = false;
            for (var j = 0; j < $scope.categories.length; j++) {
                if ($scope.categories[j].id === allCategories[i].id) {
                    allCategories[i].selected = true;
                }
            }
        }
        deferred.resolve(allCategories);
        return deferred.promise;
    }

    /*
     * @param {type} cats
     * @returns {undefined}
     * @description Push the product's categories to the server
     */
    function updateCategories(cats) {
        $scope.updatingCategories = true;
        var jobs = [];
        for (var i = 0; i < cats.length; i++) {
            if (cats[i].selected) {
                jobs.push($scope.product.putCategory(cats[i]));
            } else {
                jobs.push($scope.product.deleteCategory(cats[i]));
            }
        }
        return $q.all(jobs)
        .then(function () {
            $scope.updatingCategories = false;
            loadCategories();
        });
    }

    /*
     * تمام امکاناتی که در لایه نمایش ارائه می‌شود در اینجا نام گذاری شده است.
     */
    $scope.remove = remove;
    $scope.update = update;
    $scope.ctrl = ctrl;
    $scope.removeMetafield = removeMetafield;
    $scope.addMetafield = addMetafield;
    $scope.updateMetafield = updateMetafield;
    $scope.inlineUpdateMetafield = inlineUpdateMetafield;
    $scope.selectCategories = selectCategories;

    loadProduct();
});


'use strict';

angular.module('ngMaterialDashboardShop')

/**
 * @ngdoc Controller
 * @name AmdShopServiceCtrl
 * @description Controller of a service
 */
.controller('AmdShopServiceCtrl', function (
        $scope, $routeParams, $translate, $location, $navigator, $q, 
        /* wb-core   */ $resource,
        /* seen-core */ QueryParameter,
        /* seen-shop */ $shop, ShopService, ShopCategory, ServiceMetafield) {

    this.loading = false;
    this.updating = false;
    this.edit = false;
    this.loadingMetas = false;
    this.loadingCategories = false;
    var ctrl = this;

    /**
     * @name loadService
     * @memberOf AmdShopServiceCtrl
     * @description Load the selected service
     */
    function loadService() {
        if (ctrl.loading) {
            return;
        }
        ctrl.loading = true;
        $shop.getService($routeParams.serviceId,{
            graphql: '{id,title,description,price,off,avatar,categories{id,name,description,thumbnail}, metafields{id,key,value,unit,namespace}}'
        })//
        .then(function (p) {
            loadMetafields(p.metafields);
            loadCategories(p.categories);
            delete p.metafields;
            delete p.categories;
            $scope.service = new ShopService(p);
        }, function () {
            alert($translate.instant('Faild to load the service.'));
        })//
        .finally(function () {
            ctrl.loading = false;
        });
    }

    /**
     * @name remove
     * @memberOf AmdShopServiceCtrl
     * @description remove the selected service from the server
     */
    function remove() {
        confirm($translate.instant('Item will be deleted. There is no undo action.'))//
        .then(function () {
            return $scope.service.delete()//
            .then(function () {
                $location.path('/services/');
            }, function () {
                alert($translate.instant('Fail to delete the service.'));
            });
        });
    }

    /**
     * @name update
     * @memberOf AmdShopServiceCtrl
     * @description Update the selected service
     */
    function update() {
        if (ctrl.loading) {
            return ctrl.loading;
        }
        ctrl.loading = $scope.service.update()//
        .then(function (newService) {
            $scope.service = newService;
        }, function () {
            alert($translate.instant('Failed to update service.'));
        })//
        .finally(function () {
            ctrl.loading = false;
        });
        return ctrl.loading;
    }

    /**
     * @name loadMetas
     * @memberOf AmdShopProductCtrl
     * @description Load the metadatas of the service
     */
    function loadMetafields(list) {
        var metaFields = [];
        _.forEach(list, function(item){
            metaFields.push(new ServiceMetafield(item));
        });
        $scope.metafeilds = metaFields;
    }

    /*
     * Load categories the service belongs to.
     */
    function loadCategories(list) {
        var categories = [];
        _.forEach(list, function(item){
            categories.push(new ShopCategory(item));
        });
        $scope.categories = categories;
    }

    /**
     * @name removeMetaField
     * @memberOf AmdShopProductCtrl
     * @description Remove a metadata from the metadatas of the service
     * @param {type}
     *            metaData
     */
    function removeMetafield(metaData) {
        confirm($translate.instant('Item will be deleted. There is no undo action.'))//
        .then(function () {
            return $scope.service.deleteMetafield(metaData)//
            .then(function () {
                loadMetafields();
                toast($translate.instant('Item is deleted successfully.'));
            }, function () {
                alert($translate.instant('Failed to delete item.'));
            });
        });
    }

    function addMetafield(metadata) {
        var mydata = metadata ? metadata : {};
        $navigator.openDialog({
            templateUrl: 'views/dialogs/metafield-new.html',
            config: {
                data: mydata
            }
        // Create content
        }).then(function (meta) {
            return $scope.service.putMetafield(meta)//
            .then(function () {
                loadMetafields();
            }, function () {
                alert($translate.instant('Failed to add new item.'));
            });
        });
    }

    function updateMetafield(metadata) {
        $navigator.openDialog({
            templateUrl: 'views/dialogs/metafield-update.html',
            config: {
                data: metadata
            }
        // Create content
        }).then(function (meta) {
            return $scope.service.putMetafield(meta)//
            .then(function () {
                loadMetafields();
            }, function () {
                alert($translate.instant('Failed to update item.'));
            });
        });
    }

    function inlineUpdateMetafield(metadata) {
        return $scope.service.putMetafield(metadata)//
        .then(function () {
            loadMetafields();
        }, function () {
            alert($translate.instant('Failed to update item.'));
        });
    }

    /*
     * Assign some categories to the service.
     */
    function selectCategories() {
        $resource.get('/shop/categories', {
            // TODO: maso, 2020: add initial resources
        })
        .then(function (newCats) {
            addCategories(newCats);
        });
    }

    /*
     * @param {type} cats
     * @returns {undefined}
     * @description Push the service's categories to the server
     */
    function addCategories(cats) {
        $scope.updatingCategories = true;
        var jobs = [];
        _.forEach(cats, function(item) {
            jobs.push($scope.service.putCategory(item)
                    .then(function(){
                        $scope.categories.push(item);
                    }));
        });
        return $q.all(jobs)
        .finally(function () {
            $scope.updatingCategories = false;
        });
    }

    this.removeCategories = function() {
        $scope.updatingCategories = true;
        var jobs = [];
        _.forEach(arguments, function(item) {
            jobs.push($scope.service.deleteCategory(item)
                    .then(function(){
                        _.remove($scope.categories, function(currentObject) {
                            return currentObject.id === item.id;
                        });
                    }));
        });
        return $q.all(jobs)
        .finally(function () {
            $scope.updatingCategories = false;
        });
    }


    /*
     * تمام امکاناتی که در لایه نمایش ارائه می‌شود در اینجا نام گذاری شده است.
     */
    $scope.remove = remove;
    $scope.update = update;
    $scope.removeMetafield = removeMetafield;
    $scope.addMetafield = addMetafield;
    $scope.updateMetafield = updateMetafield;
    $scope.inlineUpdateMetafield = inlineUpdateMetafield;
    $scope.selectCategories = selectCategories;

    loadService();
});


/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

angular.module('ngMaterialDashboardShop')

/**
 * @ngdoc function
 * @name ngMaterialDashboardShop.controller:AmdShopTagCtrl
 * @description # TagCtrl Controller of the saasdmCpanelApp
 */
.controller('AmdShopTagCtrl', function($scope, $shop, $translate, $routeParams, $location) {

    var ctrl = {
            loading : false,
            saving : false,
            edit: false,
            items: []
    };

    function loadTag(){
        if(ctrl.loading){
            return;
        }
        ctrl.loading = true;
        $shop.getTag($routeParams.tagId)//
        .then(function(t){
            $scope.tag = t;
        }, function(){
            alert($translate.instant('Failed to load the tag.'));
        })//
        .finally(function(){
            ctrl.loading = false;
        });
    }
    
    function remove() {
        confirm($translate.instant('Item will be deleted. There is no undo action.'))//
        .then(function () {
            return $scope.tag.delete();//
        })//
        .then(function(){
            $location.path('/tags');
        }, function(){
            alert($translate.instant('Fail to delete the tag.'));
        });
    }

    function save(){
        if(ctrl.saving){
            return;
        }
        ctrl.saving = true;
        $scope.tag.update()//
        .then(function (newTag) {
            $scope.tag = newTag;
            ctrl.edit = false;
        }, function () {
            alert($translate.instant('Failed to save the tag.'));
        })//
        .finally(function(){
            ctrl.saving = false;
        });
    }

    $scope.tag = {};
    $scope.remove = remove;
    $scope.save = save;
    $scope.ctrl = ctrl;
    
    loadTag();
});


/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

angular.module('ngMaterialDashboardShop')

/**
 * @ngdoc Controller
 * @name AmdShopZoneCtrl
 * @description Manages a zone from shop domain
 */
.controller('AmdShopZoneCtrl', function(
    /* angularjs  */ $scope, $controller, $element,
    /* ngRoute    */ $routeParams,
    /* seen-shp   */ $shop,
    /* mblowfish  */ $actions) {

    // province, city, address, polygon, deleted, owner_id, modif_dtime, creation_dtime

    angular.extend(this, $controller('MbSeenAbstractItemCtrl', {
        $scope : $scope,
        $element: $element
    }));

    /**
	 * Deletes model
	 * 
	 * @param item
	 * @return promiss to delete item
	 * @memberof AmdShopZoneCtrl
     * @see MbSeenAbstractItemCtrl
	 */
	this.deleteModel = function(item){
		return $shop.deleteZone(item.id);
	};

	/**
	 * Gets item schema
	 * 
	 * @return promise to get schema
	 * @memberof AmdShopZoneCtrl
     * @see MbSeenAbstractItemCtrl
	 */
	this.getModelSchema = function(){
		return $shop.zoneSchema();
	};

	/**
	 * Query and get items
	 * 
	 * @param queryParameter to apply search
	 * @return promiss to get items
	 * @memberof AmdShopZoneCtrl
     * @see MbSeenAbstractItemCtrl
	 */
	this.getModel = function(id){
		return $shop.getZone(id);
	};

	/**
	 * Update current item
	 * 
	 * @return promiss to add and return an item
	 * @memberof AmdShopZoneCtrl
     * @see MbSeenAbstractItemCtrl
	 */
	this.updateModel = function(item){
		return item.update();
	};


    

    this.init({
        modelId: $routeParams.zoneId,
        eventType: '/shop/zones'
    });
});



/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
angular.module('mblowfish-core')

/**
 * @ngdoc Controllers
 * @name MbSeenShopAgenciesCtrl
 * @description Manages list of agencies from shop domain
 * 
 */
.controller('MbSeenShopAgenciesCtrl', function (
        /* angularjs */ $scope, $controller, $element,
        /* seen-shop */ $shop,
        /* mblowfish */ $actions) {

    angular.extend(this, $controller('MbSeenAbstractCollectionCtrl', {
        $scope : $scope,
        $element: $element
    }));

    // Override the function
    this.getModelSchema = function () {
        return $shop.agencySchema();
    };

    // get accounts
    this.getModels = function (parameterQuery) {
        return $shop.getAgencies(parameterQuery);
    };

    // get an account
    this.getModel = function (id) {
        return $shop.getAgency(id);
    };

    // delete account
    this.deleteModel = function (model) {
        return $shop.deleteAgency(model.id);
    };

    this.init({
        eventType: '/shop/agencies',
        actions:[{
            title: 'New Agency',
            icon: 'add',
            action: function(){
                $actions.exec('create:/shop/agencies');
            }
        }]
    });
});
/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
angular.module('mblowfish-core')

/**
 * @ngdoc Controllers
 * @name MbSeenShopCategoriesCtrl
 * @description Manages list of categories
 * 
 * 
 */
.controller('MbSeenShopCategoriesCtrl', function (
        /* angularjs */ $scope, $controller, $element,
        /* seen-shop */ $shop,
        /* mblowfish */ $actions) {

    angular.extend(this, $controller('MbSeenAbstractCollectionCtrl', {
        $scope : $scope,
        $element: $element
    }));

    // Override the function
    this.getModelSchema = function () {
        return $shop.categorySchema();
    };

    // get accounts
    this.getModels = function (parameterQuery) {
        return $shop.getCategories(parameterQuery);
    };

    // get an account
    this.getModel = function (id) {
        return $shop.getCategory(id);
    };

    // delete account
    this.deleteModel = function (model) {
        return $shop.deleteCategory(model.id);
    };

    this.init({
        eventType: '/shop/categories',
        actions:[{
            title: 'New tag',
            icon: 'add',
            action: function(){
                $actions.exec('create:/shop/categories');
            }
        }]
    });
});
/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

angular.module('mblowfish-core')

/**
 * @ngdoc Controllers
 * @name MbSeenShopDeliversCtrl
 * @description Manages list of shop delivers
 * 
 */
.controller('MbSeenShopDeliversCtrl', function (
        /* angularjs  */ $scope, $controller, $element,
        /* seen-shp   */ $shop,
        /* mblowfish  */ $actions) {

    angular.extend(this, $controller('MbSeenAbstractCollectionCtrl', {
        $scope : $scope,
        $element: $element
    }));

    // Override the function
    this.getModelSchema = function () {
        return $shop.deliverSchema();
    };

    // get accounts
    this.getModels = function (parameterQuery) {
        return $shop.getDelivers(parameterQuery);
    };

    // get an account
    this.getModel = function (id) {
        return $shop.getDeliver(id);
    };

    // delete account
    this.deleteModel = function (model) {
        return $shop.deleteDeliver(model.id);
    };

    /*************************************************************
     * Load the controller and more actions
     *************************************************************/
    this.init({
        eventType: '/shop/delivers',
        actions:[{
            title: 'New Deliver',
            icon: 'add',
            action: function () {
                $actions.exec('create:/shop/delivers');
            }
        }]
    });
});
/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


angular.module('mblowfish-core')

/**
 * @ngdoc Controllers
 * @name MbSeenShopOrdersCtrl
 * @description Manages list of categories
 * 
 * 
 */
.controller('MbSeenShopOrdersCtrl', function (
        /* angularjs */ $scope, $controller, $element, 
        /* seen-shop */ $shop,
        /* mblowfish */ $actions) {

    angular.extend(this, $controller('MbSeenAbstractCollectionCtrl', {
        $scope : $scope,
        $element: $element
    }));

    // Override the function
    this.getModelSchema = function () {
        return $shop.orderSchema();
    };

    // get accounts
    this.getModels = function (parameterQuery) {
        return $shop.getOrders(parameterQuery);
    };

    // get an account
    this.getModel = function (id) {
        return $shop.getOrder(id);
    };

    // delete account
    this.deleteModel = function (model) {
        return $shop.deleteOrder(model.id);
    };

    this.init({
        eventType: '/shop/orders',
        actions:[{
            title: 'New order',
            icon: 'add',
            action: function(){
                $actions.exec('create:/shop/orders');
            }
        }]
    });
});
/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


angular.module('mblowfish-core')

/**
 * @ngdoc Controllers
 * @name MbSeenShopProductsCtrl
 * @description Manages list of categories
 * 
 * 
 */
.controller('MbSeenShopProductsCtrl', function (
        /* angularjs  */ $scope, $controller,
        /* seen-shp   */ $shop,
        /* mblowfish  */ $actions) {

    angular.extend(this, $controller('MbSeenAbstractCollectionCtrl', {
        $scope : $scope
    }));

    // Override the function
    this.getModelSchema = function () {
        return $shop.productSchema();
    };

    // get accounts
    this.getModels = function (parameterQuery) {
        return $shop.getProducts(parameterQuery);
    };

    // get an account
    this.getModel = function (id) {
        return $shop.getProcudt(id);
    };

    // delete account
    this.deleteModel = function (model) {
        return $shop.deleteProduct(model.id);
    };

    this.init({
        eventType: '/shop/products',
        actions:[{
            title: 'New product',
            icon: 'add',
            action: function(){
                $actions.exec('create:/shop/products');
            }
        }]
    });
});
/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


angular.module('mblowfish-core')

/**
 * @ngdoc Controllers
 * @name MbSeenShopCategoriesCtrl
 * @description Manages list of categories
 * 
 */
.controller('MbSeenShopServicesCtrl', function (
        /* angularjs  */ $scope, $controller,
        /* seen-shp   */ $shop,
        /* mblowfish  */ $actions) {

    angular.extend(this, $controller('MbSeenAbstractCollectionCtrl', {
        $scope : $scope
    }));

    // Override the function
    this.getModelSchema = function () {
        return $shop.serviceSchema();
    };

    // get accounts
    this.getModels = function (parameterQuery) {
        return $shop.getServices(parameterQuery);
    };

    // get an account
    this.getModel = function (id) {
        return $shop.getService(id);
    };

    // delete account
    this.deleteModel = function (model) {
        return $shop.deleteService(model.id);
    };


    /*************************************************************
     * 
     *************************************************************/
    this.init({
        eventType: '/shop/services',
        actions:[{
            title: 'New service',
            icon: 'add',
            action: function () {
                $actions.exec('create:/shop/services');
            }
        }]
    });
});
/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


angular.module('mblowfish-core')

/**
 * @ngdoc Controllers
 * @name MbSeenShopCategoriesCtrl
 * @description Manages list of categories
 * 
 * 
 */
.controller('MbSeenShopTagsCtrl', function (
        /* angularjs */ $scope, $controller, $element,
        /* seen-shop */ $shop,
        /* mblowfish */ $actions) {

    angular.extend(this, $controller('MbSeenAbstractCollectionCtrl', {
        $scope : $scope,
        $element: $element
    }));

    // Override the function
    this.getModelSchema = function () {
        return $shop.tagSchema();
    };

    // get accounts
    this.getModels = function (parameterQuery) {
        return $shop.getTags(parameterQuery);
    };

    // get an account
    this.getModel = function (id) {
        return $shop.getTag(id);
    };

    // delete account
    this.deleteModel = function (model) {
        return $shop.deleteTag(model.id);
    };

    /***********************************************************
     * Initialize the controller
     ***********************************************************/
    this.init({
        eventType: '/shop/tags',
        actions:[{
            title: 'New tag',
            icon: 'add',
            action: function () {
                $actions.exec('create:/shop/tags');
            }
        }]
    });
});
/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
angular.module('mblowfish-core')

/**
 * @ngdoc Controllers
 * @name MbSeenShopZonesCtrl
 * @description Manages list of zones from shop domain
 * 
 * In the shop domain you are allowed to categorize agances into zones. This is a
 * main controller to manage list of zones.
 * 
 */
.controller('MbSeenShopZonesCtrl', function (
        /* angularjs */ $scope, $controller, $element,
        /* seen-shop */ $shop,
        /* mblowfish */ $actions) {

    angular.extend(this, $controller('MbSeenAbstractCollectionCtrl', {
        $scope : $scope,
        $element: $element
    }));

    // Override the function
    this.getModelSchema = function () {
        return $shop.zoneSchema();
    };

    // get accounts
    this.getModels = function (parameterQuery) {
        return $shop.getZones(parameterQuery);
    };

    // get an account
    this.getModel = function (id) {
        return $shop.getZone(id);
    };

    // delete account
    this.deleteModel = function (model) {
        return $shop.deleteZone(model.id);
    };

    this.init({
        eventType: '/shop/zones',
        actions:[{
            title: 'New Zone',
            icon: 'add',
            action: function(){
                $actions.exec('create:/shop/zones');
            }
        }]
    });
});
/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
angular.module('ngMaterialDashboardShop')
.run(function(
    /* angularjs    */ $window,
    /* am-wb-core   */ $dispatcher,
    /* mblowfish    */ $navigator, $actions,
    /* sen-shop     */ $shop,
    /* ng-translate */ $translate) {
    // TODO: maso, 2020: add zone action
    
    var actions = [{
        id: 'create:/shop/agencies',
        priority: 10,
        icon: 'store',
        title: 'New Agency',
        description: 'Creates new agency in shop domain',
        /*
         * @ngInject
         */
        action: function ($event) {
            var job = $navigator.openDialog({
                templateUrl: 'views/dialogs/amd-shop-agency-new.html',
                config: {}
            })
            .then(function (agencyData) {
                return $shop.putAgency(agencyData);
            })
            .then(function (agency) {
                $dispatcher.dispatch('/shop/agencies', {
                    key: 'create',
                    values: [agency]
                });
            }, function () {
                $window.alert($translate.instant('Failed to create new agency.'));
            });
            // TODO: maso, 2020: add the job into the job lists
            return job;
        },
        groups: ['amd.shop.agencies']
    }];

    _.forEach(actions, function(action){
        $actions.newAction(action);
    });
});
/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
angular.module('ngMaterialDashboardShop')
.run(function(
        /* angularjs    */ $window,
        /* am-wb-core   */ $dispatcher,
        /* mblowfish    */ $navigator, $actions,
        /* sen-shop     */ $shop,
        /* ng-translate */ $translate) {

    var actions = [{// create new category menu
        id: 'create:/shop/categories',
        priority: 10,
        icon: 'photo_album',
        title: 'New Category',
        description: 'Creates new category',
//      visible: function(){
//      return workbench.isContentEditable();
//      },
        /*
         * @ngInject
         */
        action: function ($event) {
            var job = $navigator.openDialog({
                templateUrl: 'views/dialogs/amd-shop-category-new.html',
                config: {}
            })
            .then(function (newConfig) {
                newConfig.parent_id = $event.parent_id;
                return $shop.putCategory(newConfig);
            })
            .then(function (cat) {
                $dispatcher.dispatch('/shop/categories', {
                    key: 'create',
                    values: [cat]
                });
            }, function () {
                $window.alert($translate.instant('Failed to create new category.'));
            });
            // TODO: maso, 2020: add the job into the job lists
            // $app.addJob('Adding new shop category', job);
            return job;
        },
        groups: ['amd.shop.categories']
    }];

    _.forEach(actions, function(action){
        $actions.newAction(action);
    });
});
/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
angular.module('ngMaterialDashboardShop')
.run(function(
        /* angularjs    */ $window,
        /* am-wb-core   */ $dispatcher,
        /* mblowfish    */ $navigator, $actions,
        /* sen-shop     */ $shop,
        /* ng-translate */ $translate) {

    var actions = [{// create new category menu
        id: 'create:/shop/delivers',
        priority: 10,
        icon: 'photo_album',
        title: 'New Deliver',
        description: 'Creates new delivers',
//      visible: function(){
//      return workbench.isContentEditable();
//      },
        /*
         * @ngInject
         */
        action: function ($event) {
            var job = $navigator.openDialog({
                templateUrl: 'views/dialogs/amd-shop-deliver-new.html',
                config: {}
            })
            .then(function (deliverData) {
                return $shop.putDeliver(deliverData);
            })
            .then(function (deliver) {
                $dispatcher.dispatch('/shop/delivers', {
                    key: 'create',
                    values: [deliver]
                });
            }, function () {
                $window.alert($translate.instant('Failed to create new deliver.'));
            });
            // TODO: maso, 2020: add the job into the job lists
            // $app.addJob('Adding new shop deliver', job);
            return job;
        },
        groups: ['amd.shop.delivers']
    }];

    _.forEach(actions, function(action){
        $actions.newAction(action);
    });
});
/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
angular.module('ngMaterialDashboardShop')
.run(function(
        /* angularjs    */ $window,
        /* am-wb-core   */ $dispatcher,
        /* mblowfish    */ $navigator, $actions,
        /* sen-shop     */ $shop,
        /* ng-translate */ $translate) {

    var actions = [{// create new category menu
        id: 'create:/shop/delivers',
        priority: 10,
        icon: 'photo_album',
        title: 'New Deliver',
        description: 'Creates new delivers',
        /*
         * @ngInject
         */
        action: function ($event) {
            $window.alert('Not supported');
        },
        groups: ['amd.shop.delivers']
    }];

    _.forEach(actions, function(action){
        $actions.newAction(action);
    });
});
/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
angular.module('ngMaterialDashboardShop')
.run(function($navigator, $actions, $shop, $dispatcher) {
    var groupId = '/shop/products';

    var actions = [{// create new category menu
        id: 'create:'+ groupId,
        priority: 10,
        icon: 'photo_album',
        title: 'New Product',
        description: 'Creates new product for shop',
        /*
         * @ngInject
         */
        action: function (/*$event*/) {
            var job = $shop.productSchema()
            .then(function(schema){
                return $navigator.openDialog({
                    templateUrl: 'views/dialogs/amd-item-new.html',
                    config: {
                        title: 'New Product',
                        schema: schema,
                        data: {}
                    }
                });
            })
            .then(function (productData) {
                return $shop.putProduct(productData);
            })
            .then(function (product) {
                $dispatcher.dispatch(groupId, {
                    key: 'create',
                    values: [product]
                });
            }, function () {
                $window.alert($translate.instant('Failed to create a new product.'));
            });
            // TODO: maso, 2020: add the job into the job lists
            // $app.addJob('Adding new shop category', job);
            return job;
        },
        groups: [groupId]
    }];

    _.forEach(actions, function(action){
        $actions.newAction(action);
    });
});
/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
angular.module('ngMaterialDashboardShop')
.run(function(
        /* angularjs    */ $window,
        /* am-wb-core   */ $dispatcher,
        /* mblowfish    */ $navigator, $actions,
        /* sen-shop     */ $shop,
        /* ng-translate */ $translate) {

    var actions = [{// create new category menu
        id: 'create:/shop/services',
        priority: 10,
        icon: 'photo_album',
        title: 'New Deliver',
        description: 'Creates new delivers',
        /*
         * @ngInject
         */
        action: function (/*$event*/) {
            var job = $shop.serviceSchema()
            .then(function(schema){
                return $navigator.openDialog({
                    templateUrl: 'views/dialogs/amd-item-new.html',
                    config: {
                        title: 'New Service',
                        schema: schema,
                        data: {}
                    }
                });
            })
            .then(function (itemData) {
                return $shop.putProduct(itemData);
            })
            .then(function (item) {
                $dispatcher.dispatch('/shop/services', {
                    key: 'create',
                    values: [item]
                });
            }, function () {
                $window.alert($translate.instant('Failed to create a new service.'));
            });
            // TODO: maso, 2020: add the job into the job lists
            // $app.addJob('Adding new shop category', job);
            return job;
        },
        groups: ['/shop/services']
    }];

    _.forEach(actions, function(action){
        $actions.newAction(action);
    });
});
/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
angular.module('ngMaterialDashboardShop')
.run(function($navigator, $actions) {
    
});
/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
angular.module('ngMaterialDashboardShop')
.run(function(
        /* angularjs    */ $window,
        /* am-wb-core   */ $dispatcher,
        /* mblowfish    */ $navigator, $actions,
        /* sen-shop     */ $shop,
        /* ng-translate */ $translate) {
    // TODO: maso, 2020: add zone action

    var actions = [{
        id: 'create:/shop/zones',
        priority: 10,
        icon: 'photo_album',
        title: 'New Category',
        description: 'Creates new category',
        /*
         * @ngInject
         */
        action: function (/*$event*/) {
            var job = $shop.zoneSchema()
            .then(function(schema){
                return $navigator.openDialog({
                    templateUrl: 'views/dialogs/amd-item-new.html',
                    config: {
                        title: 'New Zone',
                        schema: schema,
                        data: {}
                    }
                });
            })
            .then(function (zoneData) {
                return $shop.putZone(zoneData);
            })
            .then(function (zone) {
                $dispatcher.dispatch('/shop/zones', {
                    key: 'create',
                    values: [zone]
                });
            }, function () {
                $window.alert($translate.instant('Failed to create new zone.'));
            });
            // TODO: maso, 2020: add the job into the job lists
            // $app.addJob('Adding new shop category', job);
            return job;
        },
        groups: ['amd.shop.zones']
    }];

    _.forEach(actions, function(action){
        $actions.newAction(action);
    });
});
/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
angular.module('ngMaterialDashboardShop')
.run(function($navigator) {
	$navigator
	.newGroup({
		id: 'shop-management',
		title: 'Shop management',
		description: 'Manage all products, services, categories and tags in the Shop.',
		icon: 'shop',
		hidden: '!app.user.tenant_owner',
		priority: 5
	});
});

angular.module('ngMaterialDashboardShop')
/**
 * دریچه‌های محاوره‌ای
 */
.run(function($resource) {
	// Resource for list of shop categories
	$resource.newPage({
		label: 'Categories',
		type: '/shop/categories',
		templateUrl: 'views/resources/amd-shop-categories.html',
		/*
		 * @ngInject
		 */
		controller: function ($scope) {
			// TODO: maso, 2018: load selected item
			$scope.multi = true;
			this.value = $scope.value;
			this.items ={};
			this.setSelected = function (item, selected) {
				if(_.isUndefined(selected)){
					selected = true;
				}
				item._selected = selected;
				$scope.$parent.setValue(this.getSelection());
			};
			// this._setSelected = setSelected;
			this.isSelected = function(item){
				this.items[item.id] = item;
				return item._selected;
			};
			this.getSelection = function(){
				var selection = [];
				_.forEach(this.items, function(item){
					if(item._selected){
						selection.push(item);
					}
				});
				return selection;
			};
		},
		controllerAs: 'resourceCtrl',
		priority: 8,
		tags: ['/shop/categories']
	});
	
	
	
	// Zone ID resource
	$resource.newPage({
	    label: 'Zone',
	    type: '/shop/zones#id',
	    templateUrl: 'views/resources/amd-shop-zones.html',
	    /*
	     * @ngInject
	     */
	    controller: function ($scope) {
            // TODO: maso, 2018: load selected item
            $scope.multi = false;
            this.value = $scope.value;
            this.setSelected = function (item) {
                $scope.$parent.setValue(item.id);
                $scope.$parent.answer();
            };
            this.isSelected = function (item) {
                return item.id === this.value;
            };
	    },
	    controllerAs: 'resourceCtrl',
	    priority: 8,
	    tags: ['/shop/zones#id', 'zone_id']
	});
});
angular.module('ngMaterialDashboardShop').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('views/amd-shop-agencies.html',
    "<div ng-controller=\"MbSeenShopAgenciesCtrl as ctrl\" mb-preloading=ctrl.loading layout=column flex> <mb-pagination-bar mb-model=ctrl.queryParameter mb-reload=ctrl.reload() mb-sort-keys=ctrl.getSortKeys() mb-more-actions=ctrl.getActions() mb-properties=ctrl.properties> </mb-pagination-bar> <md-content mb-infinate-scroll=ctrl.loadNextPage() layout=column flex> <md-list flex> <md-list-item ng-repeat=\"pobject in ctrl.items track by pobject.id\" class=md-3-line ng-href=\"{{'shop/agencies/' + pobject.id}}\" style=\"cursor: pointer\"> <img ng-src={{pobject.avatar}} ng-if=pobject.avatar class=\"md-avatar\"> <wb-icon ng-if=!pobject.avatar>cloud_upload</wb-icon> <div class=md-list-item-text layout=column> <h3>{{pobject.title}}</h3> <h4> <span translate=\"\">Price</span>: {{pobject.price}} <span ng-show=pobject.off>- <span tanslate=\"\">Off</span>: <s>{{pobject.off}}</s></span> </h4> <p>{{pobject.description}}</p> </div> <md-divider md-inset></md-divider> </md-list-item> </md-list> </md-content> </div>"
  );


  $templateCache.put('views/amd-shop-agency.html',
    "<md-content ng-controller=\"AmdShopAgencyCtrl as ctrl\" class=md-padding layout-padding flex> <mb-titled-block ng-show=!ctrl.edit mb-progress=ctrl.loading mb-title=\"{{'Tag'| translate}}\" layout=column> <table> <tr> <td translate=\"\">ID </td> <td>: {{ctrl.item.id}}</td> </tr> <tr> <td translate=\"\">Name </td> <td><mb-inline ng-model=ctrl.item.title mb-inline-enable={{true}} mb-inline-type=text mb-inline-on-save=ctrl.updateItem()>: {{ctrl.item.title}}</mb-inline></td> </tr> <tr> <td translate=\"\">Description </td> <td><mb-inline ng-model=ctrl.item.description mb-inline-enable={{true}} mb-inline-type=text mb-inline-on-save=ctrl.updateItem()>: {{ctrl.item.description}}</mb-inline></td> </tr> <tr> <td translate=\"\">Creation Date </td> <td>: {{ctrl.item.creation_dtime| mbDate:'jYYYY-jMM-jDD'}}</td> </tr> <tr> <td translate=\"\">Last Modified Date </td> <td>: {{ctrl.item.modif_dtime| mbDate:'jYYYY-jMM-jDD'}}</td> </tr> </table>  <div ng-show=!ctrl.edit layout-gt-xs=row layout=column> <span flex></span> <md-button class=\"md-raised md-accent\" ng-click=ctrl.deleteItem()> <span translate=\"\">Delete</span> </md-button>      </div> </mb-titled-block> </md-content>"
  );


  $templateCache.put('views/amd-shop-categories.html',
    "<div ng-controller=\"MbSeenShopCategoriesCtrl as ctrl\" mb-preloading=ctrl.loading layout=column flex> <mb-pagination-bar mb-model=ctrl.queryParameter mb-reload=ctrl.reload() mb-sort-keys=ctrl.getSortKeys() mb-more-actions=ctrl.getActions() mb-properties=ctrl.properties> </mb-pagination-bar> <md-content mb-infinate-scroll=ctrl.loadNextPage() layout=column flex> <md-list flex> <md-list-item ng-repeat=\"pobject in ctrl.items track by pobject.id\" class=md-2-line ng-href=\"{{'shop/categories/' + pobject.id}}\" style=\"cursor: pointer\"> <img ng-src={{pobject.avatar}} ng-if=pobject.avatar class=\"md-avatar\"> <wb-icon ng-if=!pobject.avatar>folder_special</wb-icon> <div class=md-list-item-text layout=column>  <h4>{{pobject.name}}</h4> <p>{{pobject.description}}</p> </div> <md-divider md-inset></md-divider> </md-list-item> </md-list> </md-content> </div>"
  );


  $templateCache.put('views/amd-shop-category.html',
    "<md-content ng-controller=\"AmdShopCategoryCtrl as ctrl\" class=md-padding layout-padding flex> <mb-titled-block ng-show=!ctrl.edit mb-progress=\"ctrl.loading || ctrl.saving\" mb-title=\"{{'Category'| translate}}\" layout=column> <div layout=row layout-xs=column layout-align-xs=\"center center\" layout-padding> <mb-inline ng-model=category.thumbnail mb-inline-enable={{true}} mb-inline-type=image mb-inline-on-save=save()> <img width=256px height=256px ng-src-erro=resource/images/baseline-help-24px.svg ng-src=\"{{category.thumbnail}}\"> </mb-inline> <table> <tr> <td translate=\"\">ID </td> <td>: {{::category.id}}</td> </tr> <tr> <td translate=\"\">Parent </td> <td>: {{parent.name}}</td> </tr> <tr> <td translate=\"\">Name </td> <td><mb-inline ng-model=category.name mb-inline-enable={{true}} mb-inline-type=text mb-inline-on-save=save()>: {{category.name}}</mb-inline></td> </tr> <tr> <td translate=\"\">Description </td> <td><mb-inline ng-model=category.description mb-inline-enable={{true}} mb-inline-type=text mb-inline-on-save=save()>: {{category.description}}</mb-inline></td> </tr> <tr> <td translate=\"\">Content ID </td> <td><mb-inline ng-model=category.content_id mb-inline-enable={{true}} mb-inline-type=text mb-inline-on-save=save()>: {{category.content_id}}</mb-inline></td> </tr> </table> </div> <div ng-show=!ctrl.edit layout-gt-xs=row layout=column> <span flex></span> <md-button class=\"md-raised md-accent\" ng-click=remove()> <span translate=\"\">Delete</span> </md-button> </div> </mb-titled-block>  <mb-titled-block mb-progress=\"ctrl.loadingChilds || ctrl.savingCategory\" mb-title=\"{{'Subcategories'| translate}}\" layout=column mb-more-actions=categoryActions> <div layout=column ng-if=\"ctrl.subcategories.length == 0 && !ctrl.loadingChilds\"> <p style=\"text-align: center\" translate>List is empty.</p> </div> <md-list flex> <md-list-item ng-repeat=\"pobject in ctrl.subcategories track by pobject.id\" class=md-2-line ng-href=\"{{'categories/' + pobject.id}}\"> <img ng-src={{pobject.thumbnail}} ng-if=pobject.thumbnail class=\"md-avatar\"> <wb-icon ng-if=!pobject.thumbnail>folder_special</wb-icon> <div class=md-list-item-text layout=column> <h4>{{pobject.name}}</h4> <p>{{pobject.description}}</p> </div> <md-divider md-inset></md-divider> </md-list-item> </md-list> </mb-titled-block>  <mb-titled-block mb-progress=\"ctrl.loadingServices || ctrl.serviceSaving\" mb-title=\"{{'Services'| translate}}\" layout=column mb-more-actions=serviceActions> <div layout=column ng-if=\"ctrl.services.length == 0 && !ctrl.loadingServices\"> <p style=\"text-align: center\" translate>List is empty.</p> </div> <md-list flex> <md-list-item ng-repeat=\"pobject in ctrl.services track by pobject.id\" class=md-2-line ng-href=\"{{'services/' + pobject.id}}\"> <img ng-src={{pobject.avatar}} ng-if=pobject.avatar class=\"md-avatar\"> <wb-icon ng-if=!pobject.avatar>cloud_upload</wb-icon> <div class=md-list-item-text layout=column> <h3>{{pobject.title}}</h3> <p>{{pobject.description}}</p> </div> <md-divider md-inset></md-divider> </md-list-item> </md-list> </mb-titled-block>  <mb-titled-block mb-progress=\"ctrl.loadingProducts || ctrl.productSaving\" mb-title=\"{{'Products'| translate}}\" layout=column mb-more-actions=productActions> <div layout=column ng-if=\"ctrl.products.length === 0 && !ctrl.loadingProducts\"> <p style=\"text-align: center\" translate>List is empty.</p> </div> <md-list flex> <md-list-item ng-repeat=\"pobject in ctrl.products track by pobject.id\" class=md-2-line ng-href=\"{{'products/' + pobject.id}}\"> <img ng-src={{pobject.avatar}} ng-if=pobject.avatar class=\"md-avatar\"> <wb-icon ng-if=!pobject.avatar>unarchive</wb-icon> <div class=md-list-item-text layout=column> <h3>{{pobject.title}}</h3> <p>{{pobject.description}}</p> </div> <md-divider md-inset></md-divider> </md-list-item> </md-list> </mb-titled-block> </md-content>"
  );


  $templateCache.put('views/amd-shop-deliver.html',
    "<md-content class=md-padding layout-padding flex> <mb-titled-block ng-show=!ctrl.edit mb-progress=\"ctrl.loading || ctrl.updating\" mb-title=\"{{'Deliver'| translate}}\" layout=column> <div layout=row layout-xs=column layout-align-xs=\"center center\" layout-padding> <table> <tr> <td translate=\"\">ID </td> <td>: {{deliver.id}}</td> </tr> <tr> <td translate=\"\">Title </td> <td><mb-inline ng-model=deliver.title mb-inline-enable={{true}} mb-inline-type=text mb-inline-on-save=save()>: {{deliver.title}}</mb-inline></td> </tr> <tr> <td translate=\"\">Description </td> <td><mb-inline ng-model=deliver.description mb-inline-enable={{true}} mb-inline-type=text mb-inline-on-save=save()>: {{deliver.description}}</mb-inline></td> </tr> <tr> <td translate=\"\">Price </td> <td><mb-inline ng-model=deliver.price mb-inline-enable={{true}} mb-inline-type=text mb-inline-on-save=save()>: {{deliver.price}}</mb-inline></td> </tr> <tr> <td translate=\"\">Creation Date </td> <td>: {{deliver.creation_dtime| mbDate:'jYYYY-jMM-jDD'}}</td> </tr> <tr> <td translate=\"\">Last Modified Date </td> <td>: {{deliver.modif_dtime| mbDate:'jYYYY-jMM-jDD'}}</td> </tr> </table> </div> <div ng-show=!ctrl.edit layout-gt-xs=row layout=column> <span flex></span> <md-button class=\"md-raised md-accent\" ng-click=remove()> <span translate=\"\">Delete</span> </md-button>      </div> </mb-titled-block> </md-content>"
  );


  $templateCache.put('views/amd-shop-delivers.html',
    "<div ng-controller=\"MbSeenShopDeliversCtrl as ctrl\" mb-preloading=ctrl.loading layout=column flex> <mb-pagination-bar mb-model=ctrl.queryParameter mb-reload=ctrl.reload() mb-sort-keys=ctrl.getSortKeys() mb-more-actions=ctrl.getActions() mb-properties=ctrl.properties> </mb-pagination-bar> <md-content mb-infinate-scroll=ctrl.loadNextPage() layout=column flex> <md-list flex> <md-list-item ng-repeat=\"pobject in ctrl.items track by pobject.id\" class=md-2-line ng-href=\"{{'shop/delivers/' + pobject.id}}\" style=\"cursor: pointer\"> <img ng-src={{pobject.avatar}} ng-if=pobject.avatar class=\"md-avatar\"> <wb-icon ng-if=!pobject.avatar>send</wb-icon> <div class=md-list-item-text layout=column> <h3>{{pobject.title}}</h3>  <p>{{pobject.description}}</p> </div> <md-divider md-inset></md-divider> </md-list-item> </md-list> </md-content> </div>"
  );


  $templateCache.put('views/amd-shop-order.html',
    "<md-content ng-controller=\"AmdShopOrderCtrl as ctrl\" class=md-padding layout-padding flex> <div ng-if=\"ctrl.loading || ctrl.actionLoading || ctrl.actionDoing\" layout=row style=\"height: 90vh\" layout-align=\"center center\"> <md-progress-circular md-mode=indeterminate md-diameter=96 layout-align=\"center center\"> </md-progress-circular> </div>  <div id=order-main-div layout=column layout-gt-md=row>  <div id=order-left-div layout=column layout-align=\"start stretch\" flex=100 flex-gt-md=50 layout-margin>  <mb-titled-block mb-title=\"{{'Order information'| translate}}\" layout=column> <div id=order-info layout=column> <table> <tr> <td translate=\"\">ID </td> <td>: {{ctrl.order.id}}</td> </tr> <tr class=noprint> <td translate=\"\">Title </td> <td>: {{ctrl.order.title}}</td> </tr> <tr> <td translate=\"\">Full name </td> <td>: {{ctrl.order.full_name}}</td> </tr> <tr> <td translate=\"\">Phone </td> <td> <div layout=row> <p style=\"margin: 0px;padding: 0px;align-self: center\">{{ctrl.order.phone}}</p> <span flex=10></span> <md-button class=\"md-icon-button md-raised noprint\" style=\"cursor: pointer;align-self: center;margin: 0px\" ng-click=ctrl.callToCustomer();> <wb-icon aria-label=\"Call to customer\">call</wb-icon> </md-button> </div> </td> </tr> <tr> <td translate=\"\">Address </td> <td>: {{ctrl.order.province}} - {{ctrl.order.city}} - {{ctrl.order.address}}</td> </tr> <tr class=noprint> <td translate=\"\">Description </td> <td>: {{ctrl.order.description}}</td> </tr> <tr class=noprint> <td translate=\"\">State </td> <td>: <span translate=\"\">{{ctrl.order.state}}</span></td> </tr> <tr> <td translate=\"\">Modification Date </td> <td>: <span translate=\"\">{{ctrl.order.modif_dtime | mbDate}}</span></td> </tr> <tr> <td translate=\"\">Creation Date </td> <td>: <span translate=\"\">{{ctrl.order.creation_dtime | mbDate}}</span></td> </tr> </table>  <div ng-if=ctrl.actions.length id=order-possible-actions class=noprint layout=row layout-align=\"end center\" layout-wrap> <md-button ng-if=action.visible class=\"md-primary md-raised\" ng-repeat=\"action in ctrl.actions\" ng-click=ctrl.doAction(action)> {{action.title}} </md-button> </div> </div> </mb-titled-block>  <mb-titled-block mb-title=\"{{'Order metafields'| translate}}\" layout=column> <div layout=column> <table> <tr ng-repeat=\"item in ctrl.order.metafields\"> <td>{{item.key}}</td> <td>: {{item.value}}</td> </tr> </table> </div> </mb-titled-block>  <mb-titled-block mb-title=\"{{'Order items'| translate}}\" layout=column> <div id=order-items> <table align=center class=\"mb-table mb-shop-table\"> <thead> <tr md-colors=\"{color: 'primary-700'}\"> <td style=\"width: 5%\"></td> <td align=center translate=\"\" style=\"width: 60%\">Title</td> <td align=center translate=\"\" style=\"width: 10%\">Unit/price</td> <td align=center translate=\"\" style=\"width: 10%\">Count</td> <td align=center translate=\"\" style=\"width: 10%\">Price</td> </tr> </thead> <tbody> <tr ng-repeat=\"item in ctrl.order.order_items track by $index\"> <td align=center> <wb-icon ng-if=\"item.item_type === 'service'\" class=noprint>cloud_upload</wb-icon> <wb-icon ng-if=\"item.item_type === 'product'\" class=noprint>add_shopping_cart</wb-icon> <wb-icon ng-if=\"item.item_type === 'deliver'\" class=noprint>send</wb-icon> </td> <td> <p>{{item.title}}</p> </td> <td align=center> <p>{{item.price| number}}</p> </td> <td align=center> <p>{{item.count}}</p> </td> <td align=center> <p>{{item.price * item.count| number}}</p> </td> </tr> <tr style=\"border-bottom: none\"> <td style=\"width: 5%\"></td> <td align=start style=\"width: 60%\"> <h3 translate=\"\">Total price</h3> </td> <td style=\"width: 10%\"></td> <td style=\"width: 10%\"></td> <td ng-if=\"ctrl.order.order_items.length > 0\" align=center style=\"width: 10%\"> <h3>{{ctrl.totalP| number}}</h3> </td> </tr> </tbody> </table> </div> </mb-titled-block>  <mb-titled-block mb-title=\"{{'Order attachments'| translate}}\" layout=column class=noprint> <div id=order-attachments> <md-list> <md-list-item ng-repeat=\"attachment in ctrl.order.attachments\" ng-click=\"ctrl.showImage(ctrl.order, attachment)\"> <img ng-if=\"attachment.mime_type.startsWith('image')\" class=md-avatar ng-src=/api/v2/shop/orders/{{ctrl.order.id}}/attachments/{{attachment.id}}/content ng-src-error=\"resources/images/baseline-help-24px.svg\"> <img ng-if=\"!attachment.mime_type.startsWith('image')\" class=md-avatar ng-src=\"src/resources/images/unknown-file.png\"> <p>{{attachment.description}}</p> <md-button class=md-icon-button aria-label=\"Download attachment\" ng-href=/api/v2/shop/orders/{{ctrl.order.id}}/attachments/{{attachment.id}}/content target=_blank download={{attachment.file_name}}> <wb-icon>cloud_download</wb-icon> <md-tooltip><span translate=\"\">Download</span></md-tooltip> </md-button> </md-list-item> </md-list> <div ng-if=\"ctrl.order.attachments.length === 0\"> <p translate=\"\">No attachments</p> </div> </div> </mb-titled-block> </div>  <div id=order-right-div class=noprint layout=column layout-align=\"start stretch\" flex=100 flex-gt-md=50 layout-margin> <mb-titled-block mb-title=\"{{'Customer information'| translate}}\" layout=column> <md-list ng-if=\"ctrl.order.customer.id || ctrl.order.assignee\">  <div ng-if=ctrl.order.customer.id> <md-subheader> <h2 style=\"color: black\">{{'Customer info.'| translate}}</h2> </md-subheader> <md-list-item class=md-2-line> <img class=md-avatar ng-src=/api/v2/user/accounts/{{ctrl.order.customer.id}}/avatar ng-src-error=\"https://www.gravatar.com/avatar/{{ctrl.order.customer.id| wbmd5}}?d=identicon&size=150\"> <div class=md-list-item-text layout=column> <h3>{{ctrl.order.customer.profiles[0].first_name}} {{ctrl.order.customer.profiles[0].last_name}}</h3> </div> </md-list-item> </div>  <div ng-if=ctrl.order.assignee> <md-divider></md-divider>  <md-subheader ng-if=ctrl.order.assignee><h2 style=\"color: black\">{{'Responsible info.'| translate}}</h2></md-subheader> <md-list-item class=md-2-line ng-if=ctrl.order.assignee> <img class=md-avatar ng-src=/api/v2/user/accounts/{{ctrl.order.assignee.id}}/avatar ng-src-error=\"https://www.gravatar.com/avatar/{{ctrl.order.assignee.id| wbmd5}}?d=identicon&size=150\"> <div class=md-list-item-text layout=column> <h3>{{ctrl.order.assignee.profiles[0].first_name}} {{ctrl.order.assignee.profiles[0].last_name}}</h3> </div> </md-list-item> </div> </md-list> </mb-titled-block>  <mb-titled-block mb-title=\"{{'Order history'| translate}}\" layout=column> <div layout=column> <md-list ng-if=ctrl.order.histories.length flex> <md-list-item ng-repeat=\"history in ctrl.order.histories track by $index\" class=md-2-line style=\"cursor: pointer\"> <wb-icon ng-style=\"{'background-color': '#607D8B'}\" ng-if=\"history.action == 'create'\" class=md-avatar-icon alt={{history.action}}>add</wb-icon> <wb-icon ng-style=\"{'background-color': '#607D8B'}\" ng-if=\"history.action == 'accept'\" class=md-avatar-icon alt={{history.action}}>done</wb-icon> <wb-icon ng-style=\"{'background-color': '#607D8B'}\" ng-if=\"history.action == 'reject'\" class=md-avatar-icon alt={{history.action}}>clear</wb-icon> <wb-icon ng-style=\"{'background-color': '#607D8B'}\" ng-if=\"history.action == 'archive'\" class=md-avatar-icon alt={{history.action}}>drafts</wb-icon> <wb-icon ng-style=\"{'background-color': '#607D8B'}\" ng-if=\"history.action == 'done'\" class=md-avatar-icon alt={{history.action}}>done_all</wb-icon> <wb-icon ng-style=\"{'background-color': '#607D8B'}\" ng-if=\"history.action == 'pay'\" class=md-avatar-icon alt={{history.action}}>monetization_on</wb-icon> <div class=md-list-item-text layout=column> <h3 translate=\"\">{{history.action}}</h3> <p>{{history.description}}</p> </div> <md-divider ng-if=\"$index < ctrl.order.histories.length - 1\"></md-divider> </md-list-item> </md-list> </div> </mb-titled-block> </div> </div> </md-content>"
  );


  $templateCache.put('views/amd-shop-orders.html',
    "<div ng-controller=\"MbSeenShopOrdersCtrl as ctrl\" mb-preloading=ctrl.loading layout=column flex> <mb-pagination-bar mb-model=ctrl.queryParameter mb-reload=ctrl.reload() mb-sort-keys=ctrl.getSortKeys() mb-more-actions=ctrl.getActions() mb-properties=ctrl.properties> </mb-pagination-bar> <md-content mb-infinate-scroll=ctrl.loadNextPage() layout=column flex> <md-list flex> <md-list-item ng-repeat=\"pobject in ctrl.items track by pobject.id\" class=md-3-line ng-href=\"{{'shop/orders/' + pobject.id}}\" style=\"cursor: pointer\"> <img ng-src={{pobject.avatar}} ng-if=pobject.avatar class=\"md-avatar\"> <wb-icon ng-if=!pobject.avatar>event</wb-icon> <div class=md-list-item-text layout=column> <h3>{{pobject.title}} - ({{pobject.state| translate}})</h3> <h4>{{pobject.full_name}} - {{pobject.phone}}</h4> <p>{{pobject.province}} - {{pobject.city}} - {{pobject.address}}</p> </div> <md-divider md-inset></md-divider> </md-list-item> </md-list> </md-content> </div>"
  );


  $templateCache.put('views/amd-shop-product.html',
    "<md-content ng-controller=\"AmdShopProductCtrl as ctrl\" class=md-padding layout-padding flex> <mb-titled-block ng-show=!ctrl.edit mb-progress=ctrl.loading mb-title=\"{{'Product'| translate}}\" layout=column> <div layout=row layout-xs=column layout-align-xs=\"center center\" layout-padding> <mb-inline ng-model=product.avatar mb-inline-enable={{true}} mb-inline-type=image mb-inline-on-save=update()> <img width=256px height=256px ng-src=\"{{product.avatar}}\"> </mb-inline> <table> <tr> <td translate=\"\">ID </td> <td>: {{product.id}}</td> </tr> <tr> <td translate=\"\">Name </td> <td><mb-inline ng-model=product.title mb-inline-enable={{true}} mb-inline-type=text mb-inline-on-save=update()>: {{product.title}}</mb-inline></td> </tr> <tr> <td translate=\"\">Description </td> <td><mb-inline ng-model=product.description mb-inline-enable={{true}} mb-inline-type=text mb-inline-on-save=update()>: {{product.description}}</mb-inline></td> </tr> <tr> <td translate=\"\">Price </td> <td><mb-inline ng-model=product.price mb-inline-enable={{true}} mb-inline-type=text mb-inline-on-save=update()>: {{product.price}}</mb-inline></td> </tr> <tr> <td translate=\"\">Off </td> <td><mb-inline ng-model=product.off mb-inline-enable={{true}} mb-inline-type=text mb-inline-on-save=update()>: {{product.off}}</mb-inline></td> </tr> <tr> <td translate=\"\">Manufacturer </td> <td><mb-inline ng-model=product.manufacturer mb-inline-enable={{true}} mb-inline-type=text mb-inline-on-save=update()>: {{product.manufacturer}}</mb-inline></td> </tr> <tr> <td translate=\"\">Brand </td> <td><mb-inline ng-model=product.brand mb-inline-enable={{true}} mb-inline-type=text mb-inline-on-save=update()>: {{product.brand}}</mb-inline></td> </tr> <tr> <td translate=\"\">Model </td> <td><mb-inline ng-model=product.model mb-inline-enable={{true}} mb-inline-type=text mb-inline-on-save=update()>: {{product.model}}</mb-inline></td> </tr> <tr> <td translate=\"\">Creation Date </td> <td>: {{product.creation_dtime| mbDate}}</td> </tr> <tr> <td translate=\"\">Last Modified Date </td> <td>: {{product.modif_dtime| mbDate}}</td> </tr> </table> </div> <div layout-gt-xs=row layout=column> <span flex></span> <md-button class=\"md-raised md-accent\" ng-click=remove()> <span translate=\"\">Delete</span> </md-button> </div> </mb-titled-block>  <mb-titled-block ng-show=!ctrl.edit mb-progress=\"ctrl.loading || ctrl.loadingMetas\" layout=column> <table class=\"mb-table mb-shop-table\"> <thead> <tr md-colors=\"{color: 'primary-700'}\"> <td style=\"width: 20%\" translate=\"\">Field key</td> <td style=\"width: 20%\" translate=\"\">Field value</td> <td style=\"width: 20%\" translate=\"\">Unit</td> <td style=\"width: 20%\" translate=\"\">Namespace</td> <td translate style=\"width: 10%;text-align: center\"></td> </tr> </thead> <tbody> <tr ng-repeat=\"meta in metafeilds\"> <td> <am-wb-inline ng-model=meta.key am-wb-inline-enable=false am-wb-inline-type=text am-wb-inline-label=\"{{'Field key' | translate}}\" am-wb-inline-on-save=inlineUpdateMetafield(meta)> <p> {{meta.key}} </p> </am-wb-inline> </td> <td> <am-wb-inline ng-model=meta.value am-wb-inline-enable=true am-wb-inline-type=text am-wb-inline-label=\"{{'Field value' | translate}}\" am-wb-inline-on-save=inlineUpdateMetafield(meta)> <p> {{meta.value}} </p> </am-wb-inline> </td> <td> <am-wb-inline ng-model=meta.unit am-wb-inline-enable=true am-wb-inline-type=text am-wb-inline-label=\"{{'Unit' | translate}}\" am-wb-inline-on-save=inlineUpdateMetafield(meta)> <p> {{meta.unit}} </p> </am-wb-inline> </td> <td> <am-wb-inline ng-model=meta.namespace am-wb-inline-enable=true am-wb-inline-type=text am-wb-inline-label=\"{{'Namespace' | translate}}\" am-wb-inline-on-save=inlineUpdateMetafield(meta)> <p> {{meta.namespace}} </p> </am-wb-inline> </td> <td style=\"text-align: center\"> <div layout=row> <md-button class=\"md-icon-button md-primary\" ng-click=updateMetafield(meta) aria-label=\"edit metafield\"> <wb-icon>edit</wb-icon> </md-button> <md-button class=\"md-icon-button md-accent\" ng-click=removeMetafield(meta) aria-label=\"remove variable\"> <wb-icon>delete</wb-icon> </md-button> </div> </td> </tr> </tbody> </table> <div layout=row> <span flex></span> <md-button layout-align=\"end center\" class=\"md-icon-button md-raised md-primary\" ng-click=addMetafield() aria-label=\"add meta data\"> <wb-icon>add</wb-icon> </md-button> </div> </mb-titled-block>  <mb-titled-block ng-show=!ctrl.edit mb-progress=\"ctrl.loading || loadingCategories || updatingCategories\" mb-title=\"{{'Categories'| translate}}\" layout=column> <md-list flex ng-if=categories> <md-list-item ng-repeat=\"category in categories track by $index\" class=md-2-line ng-href=\"{{'categories/' + category.id}}\"> <img ng-if=category.thumbnail ng-src={{category.thumbnail}} class=\"md-avatar\"> <wb-icon ng-if=!category.thumbnail>folder_special</wb-icon> <div class=md-list-item-text layout=column> <h3>{{category.name}}</h3> <p>{{category.description}}</p> </div> <md-divider md-inset></md-divider> </md-list-item> </md-list> <div layout=column ng-if=\"!categories || categories.length === 0\"> <p style=\"text-align: center\" translate=\"\">Nothing found.</p> </div> <div layout=row> <span flex></span> <md-button class=\"md-raised md-primary\" ng-click=selectCategories()> <span translate=\"\">Edit</span> </md-button> </div> </mb-titled-block> </md-content>"
  );


  $templateCache.put('views/amd-shop-products.html',
    "<div ng-controller=\"MbSeenShopProductsCtrl as ctrl\" mb-preloading=ctrl.loading layout=column flex> <mb-pagination-bar mb-model=ctrl.queryParameter mb-reload=ctrl.reload() mb-sort-keys=ctrl.getSortKeys() mb-more-actions=ctrl.getActions() mb-properties=ctrl.properties> </mb-pagination-bar> <md-content mb-infinate-scroll=ctrl.loadNextPage() layout=column flex> <md-list flex> <md-list-item ng-repeat=\"pobject in ctrl.items track by pobject.id\" class=md-3-line ng-href=\"{{'shop/products/' + pobject.id}}\" style=\"cursor: pointer\"> <img ng-src={{pobject.avatar}} ng-if=pobject.avatar class=\"md-avatar\"> <wb-icon ng-if=!pobject.avatar>unarchive</wb-icon> <div class=md-list-item-text layout=column> <h3>{{pobject.title}}</h3> <h4> <span translate=\"\">Price</span>: {{pobject.price}} <span ng-show=pobject.off>- <span tanslate=\"\">Off</span>: <s>{{pobject.off}}</s></span> </h4> <p>{{pobject.description}}</p> </div> <md-divider md-inset></md-divider> </md-list-item> </md-list> </md-content> </div>"
  );


  $templateCache.put('views/amd-shop-service.html',
    "<md-content ng-controller=\"AmdShopServiceCtrl as ctrl\" class=md-padding layout-padding flex> <mb-titled-block ng-show=!ctrl.edit mb-progress=ctrl.loading mb-title=\"{{'Service'| translate}}\" layout=column> <div layout=row layout-xs=column layout-align-xs=\"center center\" layout-padding> <mb-inline ng-model=service.avatar mb-inline-enable={{true}} mb-inline-type=image mb-inline-on-save=update()> <img width=256px height=256px ng-src=\"{{service.avatar}}\"> </mb-inline> <table> <tr> <td translate=\"\">ID </td> <td>: {{service.id}}</td> </tr> <tr> <td translate=\"\">Title </td> <td><mb-inline ng-model=service.title mb-inline-enable={{true}} mb-inline-type=text mb-inline-on-save=update()>: {{service.title}}</mb-inline></td> </tr> <tr> <td translate=\"\">Description </td> <td><mb-inline ng-model=service.description mb-inline-enable={{true}} mb-inline-type=text mb-inline-on-save=update()>: {{service.description}}</mb-inline></td> </tr> <tr> <td translate=\"\">Price </td> <td><mb-inline ng-model=service.price mb-inline-enable={{true}} mb-inline-type=text mb-inline-on-save=update()>: {{service.price}}</mb-inline></td> </tr> <tr> <td translate=\"\">Off </td> <td><mb-inline ng-model=service.off mb-inline-enable={{true}} mb-inline-type=text mb-inline-on-save=update()>: {{service.off}}</mb-inline></td> </tr> </table> </div> <div layout-gt-xs=row layout=column> <span flex></span> <md-button class=\"md-raised md-accent\" ng-click=remove()> <span translate=\"\">Delete</span> </md-button> </div> </mb-titled-block>  <mb-titled-block ng-show=!ctrl.edit mb-title=\"{{'Meta fields'| translate}}\" mb-progress=\"ctrl.loading || ctrl.loadingMetas\" layout=column> <table class=\"mb-table mb-shop-table\"> <thead> <tr md-colors=\"{color: 'primary-700'}\"> <td style=\"width: 20%\" translate=\"\">Field key</td> <td style=\"width: 20%\" translate=\"\">Field value</td> <td style=\"width: 20%\" translate=\"\">Unit</td> <td style=\"width: 20%\" translate=\"\">Namespace</td> <td translate style=\"width: 10%;text-align: center\"></td> </tr> </thead> <tbody> <tr ng-repeat=\"meta in metafeilds\"> <td> <am-wb-inline ng-model=meta.key am-wb-inline-enable=false am-wb-inline-type=text am-wb-inline-label=\"{{'Field key' | translate}}\" am-wb-inline-on-save=inlineUpdateMetafield(meta)> <p> {{meta.key}} </p> </am-wb-inline> </td> <td> <am-wb-inline ng-model=meta.value am-wb-inline-enable=true am-wb-inline-type=text am-wb-inline-label=\"{{'Field value' | translate}}\" am-wb-inline-on-save=inlineUpdateMetafield(meta)> <p> {{meta.value}} </p> </am-wb-inline> </td> <td> <am-wb-inline ng-model=meta.unit am-wb-inline-enable=true am-wb-inline-type=text am-wb-inline-label=\"{{Unit | translate}}\" am-wb-inline-on-save=inlineUpdateMetafield(meta)> <p> {{meta.unit}} </p> </am-wb-inline> </td> <td> <am-wb-inline ng-model=meta.namespace am-wb-inline-enable=true am-wb-inline-type=text am-wb-inline-label=\"{{'Namespace' | translate}}\" am-wb-inline-on-save=inlineUpdateMetafield(meta)> <p> {{meta.namespace}} </p> </am-wb-inline> </td> <td style=\"text-align: center\"> <div layout=row> <md-button class=\"md-icon-button md-primary\" ng-click=updateMetafield(meta) aria-label=\"edit metafield\"> <wb-icon>edit</wb-icon> </md-button> <md-button class=\"md-icon-button md-accent\" ng-click=removeMetafield(meta) aria-label=\"remove variable\"> <wb-icon>delete</wb-icon> </md-button> </div> </td> </tr> </tbody> </table> <div layout=row> <span flex></span> <md-button layout-align=\"end center\" class=\"md-icon-button md-raised md-primary\" ng-click=addMetafield() aria-label=\"add meta data\"> <wb-icon>add</wb-icon> </md-button> </div> </mb-titled-block>  <mb-titled-block ng-show=!ctrl.edit mb-progress=\"ctrl.loading || loadingCategories || updatingCategories\" mb-title=\"{{'Categories'| translate}}\" layout=column> <md-list flex ng-if=categories> <md-list-item ng-repeat=\"category in categories track by $index\" class=md-2-line ng-href=\"{{'categories/' + category.id}}\"> <img ng-if=category.thumbnail ng-src={{category.thumbnail}} class=\"md-avatar\"> <wb-icon ng-if=!category.thumbnail>folder_special</wb-icon> <div class=md-list-item-text layout=column> <h3>{{category.name}}</h3> <p>{{category.description}}</p> </div> <md-icon class=md-secondary ng-click=ctrl.removeCategories(category) aria-label=delete>delete</md-icon> <md-divider md-inset></md-divider> </md-list-item> </md-list> <div layout=column ng-if=\"!categories || categories.length === 0\"> <p style=\"text-align: center\" translate=\"\">Nothing found.</p> </div> <div layout=row> <span flex></span> <md-button class=\"md-raised md-primary\" ng-click=selectCategories()> <span translate=\"\">Edit</span> </md-button> </div> </mb-titled-block> </md-content>"
  );


  $templateCache.put('views/amd-shop-services.html',
    "<div ng-controller=\"MbSeenShopServicesCtrl as ctrl\" mb-preloading=ctrl.loading layout=column flex> <mb-pagination-bar mb-model=ctrl.queryParameter mb-reload=ctrl.reload() mb-sort-keys=ctrl.getSortKeys() mb-more-actions=ctrl.getActions() mb-properties=ctrl.properties> </mb-pagination-bar> <md-content mb-infinate-scroll=ctrl.loadNextPage() layout=column flex> <md-list flex> <md-list-item ng-repeat=\"pobject in ctrl.items track by pobject.id\" class=md-3-line ng-href=\"{{'shop/services/' + pobject.id}}\" style=\"cursor: pointer\"> <img ng-src={{pobject.avatar}} ng-if=pobject.avatar class=\"md-avatar\"> <wb-icon ng-if=!pobject.avatar>cloud_upload</wb-icon> <div class=md-list-item-text layout=column> <h3>{{pobject.title}}</h3> <h4> <span translate=\"\">Price</span>: {{pobject.price}} <span ng-show=pobject.off>- <span tanslate=\"\">Off</span>: <s>{{pobject.off}}</s></span> </h4> <p>{{pobject.description}}</p> </div> <md-divider md-inset></md-divider> </md-list-item> </md-list> </md-content> </div>"
  );


  $templateCache.put('views/amd-shop-tag.html',
    "<md-content ng-controller=\"AmdShopTagCtrl as ctrl\" class=md-padding layout-padding flex> <mb-titled-block ng-show=!ctrl.edit mb-progress=ctrl.loading mb-title=\"{{'Tag'| translate}}\" layout=column> <table> <tr> <td translate=\"\">ID </td> <td>: {{tag.id}}</td> </tr> <tr> <td translate=\"\">Name </td> <td><mb-inline ng-model=tag.name mb-inline-enable={{true}} mb-inline-type=text mb-inline-on-save=save()>: {{tag.name}}</mb-inline></td> </tr> <tr> <td translate=\"\">Description </td> <td><mb-inline ng-model=tag.description mb-inline-enable={{true}} mb-inline-type=text mb-inline-on-save=save()>: {{tag.description}}</mb-inline></td> </tr> <tr> <td translate=\"\">Creation Date </td> <td>: {{tag.creation_dtime| mbDate:'jYYYY-jMM-jDD'}}</td> </tr> <tr> <td translate=\"\">Last Modified Date </td> <td>: {{tag.modif_dtime| mbDate:'jYYYY-jMM-jDD'}}</td> </tr> </table>  <div ng-show=!ctrl.edit layout-gt-xs=row layout=column> <span flex></span> <md-button class=\"md-raised md-accent\" ng-click=remove()> <span translate=\"\">Delete</span> </md-button>      </div> </mb-titled-block> </md-content>"
  );


  $templateCache.put('views/amd-shop-tags.html',
    "<div ng-controller=\"MbSeenShopTagsCtrl as ctrl\" mb-preloading=ctrl.loading layout=column flex>  <mb-pagination-bar mb-model=ctrl.queryParameter mb-reload=ctrl.reload() mb-sort-keys=ctrl.getSortKeys() mb-more-actions=ctrl.getActions() mb-properties=ctrl.properties> </mb-pagination-bar> <md-content mb-infinate-scroll=ctrl.loadNextPage() layout=column flex> <md-list flex> <md-list-item ng-repeat=\"tag in ctrl.items track by tag.id\" class=md-3-line ng-href=\"{{'shop/tags/' + tag.id}}\"> <wb-icon>label</wb-icon> <div class=md-list-item-text layout=column> <h3>{{::tag.id}} - {{::tag.name}}</h3> <p>{{::tag.description}}</p> </div> <md-divider md-inset></md-divider> </md-list-item> </md-list> </md-content> </div>"
  );


  $templateCache.put('views/amd-shop-zone.html',
    "<md-content ng-controller=\"AmdShopZoneCtrl as ctrl\" class=md-padding layout-padding flex> <mb-titled-block ng-show=!ctrl.edit mb-progress=ctrl.loading mb-title=\"{{'Tag'| translate}}\" layout=column> <table> <tr> <td translate=\"\">ID </td> <td>: {{ctrl.item.id}}</td> </tr> <tr> <td translate=\"\">Name </td> <td><mb-inline ng-model=ctrl.item.title mb-inline-enable={{true}} mb-inline-type=text mb-inline-on-save=ctrl.updateItem()>: {{ctrl.item.title}}</mb-inline></td> </tr> <tr> <td translate=\"\">Description </td> <td><mb-inline ng-model=ctrl.item.description mb-inline-enable={{true}} mb-inline-type=text mb-inline-on-save=ctrl.updateItem()>: {{ctrl.item.description}}</mb-inline></td> </tr> <tr> <td translate=\"\">Creation Date </td> <td>: {{ctrl.item.creation_dtime| mbDate:'jYYYY-jMM-jDD'}}</td> </tr> <tr> <td translate=\"\">Last Modified Date </td> <td>: {{ctrl.item.modif_dtime| mbDate:'jYYYY-jMM-jDD'}}</td> </tr> </table>  <div ng-show=!ctrl.edit layout-gt-xs=row layout=column> <span flex></span> <md-button class=\"md-raised md-accent\" ng-click=ctrl.deleteItem()> <span translate=\"\">Delete</span> </md-button>      </div> </mb-titled-block> </md-content>"
  );


  $templateCache.put('views/amd-shop-zones.html',
    "<div ng-controller=\"MbSeenShopZonesCtrl as ctrl\" mb-preloading=ctrl.loading layout=column flex> <mb-pagination-bar mb-model=ctrl.queryParameter mb-reload=ctrl.reload() mb-sort-keys=ctrl.getSortKeys() mb-more-actions=ctrl.getActions() mb-properties=ctrl.properties> </mb-pagination-bar> <md-content mb-infinate-scroll=ctrl.loadNextPage() layout=column flex> <md-list flex> <md-list-item ng-repeat=\"pobject in ctrl.items track by pobject.id\" class=md-3-line ng-href=\"{{'shop/zones/' + pobject.id}}\" style=\"cursor: pointer\"> <img ng-src={{pobject.avatar}} ng-if=pobject.avatar class=\"md-avatar\"> <wb-icon ng-if=!pobject.avatar>cloud_upload</wb-icon> <div class=md-list-item-text layout=column> <h3>{{pobject.title}}</h3> <h4> <span translate=\"\">Price</span>: {{pobject.price}} <span ng-show=pobject.off>- <span tanslate=\"\">Off</span>: <s>{{pobject.off}}</s></span> </h4> <p>{{pobject.description}}</p> </div> <md-divider md-inset></md-divider> </md-list-item> </md-list> </md-content> </div>"
  );


  $templateCache.put('views/dialogs/action-properties.html',
    "<md-dialog flex=50 layout=column ng-cloak> <md-toolbar> <div class=md-toolbar-tools> <h2 translate=\"\">Properties</h2> <span flex></span> <md-button class=md-icon-button ng-click=answer(config.action) ng-disabled=myForm.$invalid> <wb-icon aria-label=\"Close dialog\">done</wb-icon> </md-button> <md-button class=md-icon-button ng-click=cancel()> <wb-icon aria-label=\"Close dialog\">close</wb-icon> </md-button> </div> </md-toolbar> <md-dialog-content layout-padding> <mb-dynamic-form ng-model=config.action mb-parameters=config.properties> </mb-dynamic-form> </md-dialog-content> </md-dialog>"
  );


  $templateCache.put('views/dialogs/amd-item-new.html',
    "<md-dialog layout=column ng-cloak flex=50> <md-toolbar> <div class=md-toolbar-tools> <h2 translate=\"\">{{::config.title}}</h2> <span flex></span> <md-button class=md-icon-button ng-click=answer(config.data) ng-disabled=myForm.$invalid> <wb-icon aria-label=\"Close dialog\">done</wb-icon> </md-button> <md-button class=md-icon-button ng-click=cancel()> <wb-icon aria-label=\"Close dialog\">close</wb-icon> </md-button> </div> </md-toolbar> <md-dialog-content layout-padding> <mb-dynamic-form ng-model=config.data mb-parameters=config.schema.children> </mb-dynamic-form> </md-dialog-content> </md-dialog>"
  );


  $templateCache.put('views/dialogs/amd-shop-agency-new.html',
    "<md-dialog layout=column ng-cloak flex=50> <md-toolbar> <div class=md-toolbar-tools> <h2 translate=\"\">New Zone</h2> <span flex></span> <md-button class=md-icon-button ng-click=answer(config) ng-disabled=myForm.$invalid> <wb-icon aria-label=\"Close dialog\">done</wb-icon> </md-button> <md-button class=md-icon-button ng-click=cancel()> <wb-icon aria-label=\"Close dialog\">close</wb-icon> </md-button> </div> </md-toolbar> <md-dialog-content layout-padding> <form name=myForm ng-action=add(config) layout=column flex> <md-input-container> <label tranlate=\"\">Title</label> <input ng-model=config.title name=title required> <div ng-messages=myForm.name.$error> <div ng-message=required translate=\"\">This field is required.</div> </div> </md-input-container> <md-input-container> <label tranlate=\"\">Description</label> <input ng-model=config.description> </md-input-container> <md-input-container> <label tranlate=\"\">province</label> <input ng-model=config.province> </md-input-container> <md-input-container> <label tranlate=\"\">city</label> <input ng-model=config.city> </md-input-container> <md-input-container> <label tranlate=\"\">address</label> <input ng-model=config.address> </md-input-container> <md-input-container> <label tranlate=\"\">phone</label> <input ng-model=config.phone> </md-input-container> <md-input-container> <label tranlate=\"\">point</label> <input ng-model=config.point> </md-input-container> <md-input-container> <label tranlate=\"\">owner_id</label> <input ng-model=config.owner_id> </md-input-container> </form> </md-dialog-content> </md-dialog>"
  );


  $templateCache.put('views/dialogs/amd-shop-category-new.html',
    "<md-dialog layout=column ng-cloak flex=50> <md-toolbar> <div class=md-toolbar-tools> <h2 translate=\"\">New category</h2> <span flex></span> <md-button class=md-icon-button ng-click=answer(config) ng-disabled=myForm.$invalid> <wb-icon aria-label=\"Close dialog\">done</wb-icon> </md-button> <md-button class=md-icon-button ng-click=cancel()> <wb-icon aria-label=\"Close dialog\">close</wb-icon> </md-button> </div> </md-toolbar> <md-dialog-content layout-padding> <form name=myForm ng-action=add(config) layout=column flex> <md-input-container> <label tranlate=\"\">Name</label> <input ng-model=config.name name=name required> <div ng-messages=myForm.name.$error> <div ng-message=required translate=\"\">This field is required.</div> </div> </md-input-container> <md-input-container> <label tranlate=\"\">Description</label> <input ng-model=config.description> </md-input-container> <wb-ui-setting-image title=\"{{'Image' | translate}}\" wb-ui-setting-clear-button=true wb-ui-setting-preview=true ng-model=config.thumbnail> </wb-ui-setting-image> </form> </md-dialog-content> </md-dialog>"
  );


  $templateCache.put('views/dialogs/amd-shop-deliver-new.html',
    "<md-dialog layout=column ng-cloak flex=50> <md-toolbar> <div class=md-toolbar-tools> <h2 translate=\"\">New delivery</h2> <span flex></span> <md-button class=md-icon-button ng-click=answer(config) ng-disabled=myForm.$invalid> <wb-icon aria-label=\"Close dialog\">done</wb-icon> </md-button> <md-button class=md-icon-button ng-click=cancel()> <wb-icon aria-label=\"Close dialog\">close</wb-icon> </md-button> </div> </md-toolbar> <md-dialog-content layout-padding> <form name=myForm ng-action=add(config) layout=column flex> <md-input-container> <label tranlate=\"\">Title</label> <input ng-model=config.title name=title required> <div ng-messages=myForm.title.$error> <div ng-message=required translate>This field is required.</div> </div> </md-input-container> <md-input-container> <label tranlate=\"\">Description</label> <input ng-model=config.description> </md-input-container> <md-input-container> <label tranlate=\"\">Price</label> <input ng-model=config.price type=number required> <div ng-messages=myForm.title.$error> <div ng-message=required translate=\"\">This field is required.</div> </div> </md-input-container> <md-input-container> <label tranlate=\"\">Off</label> <input ng-model=config.off> </md-input-container> <wb-ui-setting-image title=\"{{'Image' | translate}}\" wb-ui-setting-clear-button=true wb-ui-setting-preview=true ng-model=config.avatar> </wb-ui-setting-image> </form> </md-dialog-content> </md-dialog>"
  );


  $templateCache.put('views/dialogs/amd-shop-service-new.html',
    "<md-dialog layout=column ng-cloak flex=50> <md-toolbar> <div class=md-toolbar-tools> <h2 translate=\"\">New service</h2> <span flex></span> <md-button class=md-icon-button ng-click=answer(config) ng-disabled=newServiceForm.$invalid> <wb-icon aria-label=\"Close dialog\">done</wb-icon> </md-button> <md-button class=md-icon-button ng-click=cancel()> <wb-icon aria-label=\"Close dialog\">close</wb-icon> </md-button> </div> </md-toolbar> <md-dialog-content layout-padding> <form name=newServiceForm ng-action=answer(config) layout=row layout-wrap layout-align=\"center center\" flex> <mb-inline ng-model=config.avatar mb-inline-enable={{true}} mb-inline-type=image> <img width=256px height=256px ng-src=\"{{config.avatar}}\"> </mb-inline> <div layout=column layout-margin> <md-input-container> <label tranlate=\"\">Title</label> <input ng-model=config.title name=title required> <div ng-messages=myForm.title.$error> <div ng-message=required translate>This field is required.</div> </div> </md-input-container> <md-input-container> <label tranlate=\"\">Description</label> <input ng-model=config.description> </md-input-container> <md-input-container> <label tranlate=\"\">Price</label> <input ng-model=config.price type=number required> <div ng-messages=myForm.title.$error> <div ng-message=required translate=\"\">This field is required.</div> </div> </md-input-container> <md-input-container> <label tranlate=\"\">Off</label> <input ng-model=config.off> </md-input-container> </div> </form> </md-dialog-content> </md-dialog>"
  );


  $templateCache.put('views/dialogs/amd-shop-tag-new.html',
    "<md-dialog layout=column ng-cloak flex=50> <md-toolbar> <div class=md-toolbar-tools> <h2 translate=\"\">New Tag</h2> <span flex></span> <md-button class=md-icon-button ng-click=answer(config) ng-disabled=myForm.$invalid> <wb-icon aria-label=\"Close dialog\">done</wb-icon> </md-button> <md-button class=md-icon-button ng-click=cancel()> <wb-icon aria-label=\"Close dialog\">close</wb-icon> </md-button> </div> </md-toolbar> <md-dialog-content layout-padding> <form name=myForm ng-action=add(config) layout=column flex> <md-input-container> <label tranlate=\"\">Name</label> <input ng-model=config.name name=name required> <div ng-messages=myForm.name.$error> <div ng-message=required translate=\"\">This field is required.</div> </div> </md-input-container> <md-input-container> <label tranlate=\"\">Description</label> <input ng-model=config.description> </md-input-container> </form> </md-dialog-content> </md-dialog>"
  );


  $templateCache.put('views/dialogs/categories.html',
    "<md-content mb-infinate-scroll=nextPage() layout=column flex>  <mb-pagination-bar mb-model=paginatorParameter mb-reload=reload mb-sort-keys=sortKeys mb-more-actions=moreActions> </mb-pagination-bar>  <div layout=column ng-if=\"ctrl.items.length === 0 && !ctrl.loadingItems\"> <p style=\"text-align: center\" translate=\"\">List is empty.</p> <div layout=row layout-align=\"center center\"> <md-button class=md-raised ng-click=add()> <wb-icon>add</wb-icon> <span translate=\"\">New category</span> </md-button> </div> </div> <md-list flex> <md-list-item ng-repeat=\"pobject in ctrl.items track by pobject.id\" class=md-2-line ng-href=\"{{'categories/'+pobject.id}}\"> <img ng-src={{pobject.avatar}} ng-if=pobject.avatar class=\"md-avatar\"> <wb-icon ng-if=!pobject.avatar>folder_special</wb-icon> <div class=md-list-item-text layout=column>  <h4>{{pobject.name}}</h4> <p>{{pobject.description}}</p> </div> <md-divider md-inset></md-divider> </md-list-item>  <div layout=column layout-align=\"center center\"> <md-progress-circular ng-show=\"ctrl.loadingItems || ctrl.savingCategory\" md-diameter=96> {{'Loading ...' | translate}} </md-progress-circular> </div> </md-list> </md-content>"
  );


  $templateCache.put('views/dialogs/category-select.html',
    "<md-dialog ng-controller=CategoriesCtrl aria-label=Category> <md-toolbar> <div class=md-toolbar-tools> <h2 translate=\"\">Select category</h2> <span flex></span> <md-button class=md-icon-button ng-click=answer(ctrl.selected)> <wb-icon aria-label=select>done</wb-icon> </md-button> <md-button class=md-icon-button ng-click=cancel()> <wb-icon aria-label=\"Close dialog\">close</wb-icon> </md-button> </div> </md-toolbar> <md-dialog-content ng-init=nextPage() amd-infinate-scroll=nextPage() layout=column layout-padding flex> <md-list flex> <md-list-item class=md-3-line ng-click=\"ctrl.selected = category\" ng-class=\"{true:'md - raised md - primary', false:''}[ctrl.selected.id === category.id]\" ng-style=\"{'background-color': ctrl.selected.id === category.id ? '#cce2ff' : ''}\" ng-repeat=\"category in ctrl.items\"> <div class=md-list-item-text layout=column> <h4>{{'ID'| translate}}: {{category.id}}</h4> <h3>{{'Name'| translate}}: {{category.name}}</h3> <h4>{{'Parent'| translate}}: {{category.parent_id}}</h4> <p>{{'Description'| translate}}: {{category.description}}</p> </div> <md-divider></md-divider> </md-list-item>  <div layout=column layout-align=\"center center\"> <md-progress-circular ng-show=\"ctrl.status === 'working'\" md-diameter=96> Loading ... </md-progress-circular> </div> </md-list> </md-dialog-content> <md-dialog-actions layout=row> <span flex></span> <form ng-submit=search(temp.query)> <md-input-container class=\"md-icon-float md-icon-right md-block\"> <label translate=\"\">Search</label> <wb-icon>search</wb-icon> <input ng-model=temp.query> </md-input-container> <input type=submit hide> </form> </md-dialog-actions> </md-dialog>"
  );


  $templateCache.put('views/dialogs/metafield-new.html',
    "<md-dialog layout=column ng-cloak flex=50> <md-toolbar> <div class=md-toolbar-tools> <h2 translate=\"\">New metadata</h2> <span flex></span> <md-button class=md-icon-button ng-click=answer(config.data) ng-disabled=myForm.$invalid> <wb-icon aria-label=\"Close dialog\">done</wb-icon> </md-button> <md-button class=md-icon-button ng-click=cancel()> <wb-icon aria-label=\"Close dialog\">close</wb-icon> </md-button> </div> </md-toolbar> <md-dialog-content> <form name=myForm layout=column layout-padding> <span flex=10></span> <md-input-container required> <label translate=\"\">Key</label> <input ng-model=config.data.key name=key required> <div ng-messages=myForm.key.$error ng-show=ctrl.showError> <div ng-message=required translate=\"\">This field is required.</div> </div> </md-input-container> <md-input-container> <label translate=\"\">Value</label> <input ng-model=config.data.value name=value required> <div ng-messages=myForm.value.$error ng-show=ctrl.showError> <div ng-message=required translate=\"\">This field is required.</div> </div> </md-input-container> <md-input-container> <label translate=\"\">Unit</label> <input ng-model=config.data.unit> </md-input-container> <md-input-container> <label translate=\"\">Namespace</label> <input ng-model=config.data.namespace> </md-input-container> </form> </md-dialog-content> </md-dialog>"
  );


  $templateCache.put('views/dialogs/metafield-update.html',
    "<md-dialog layout=column ng-cloak> <md-toolbar> <div class=md-toolbar-tools> <h2 translate=\"\">Update metadata</h2> <span flex></span> <md-button class=md-icon-button ng-click=answer(config.data) ng-disabled=myForm.$invalid> <wb-icon aria-label=\"Close dialog\">done</wb-icon> </md-button> <md-button class=md-icon-button ng-click=cancel()> <wb-icon aria-label=\"Close dialog\">close</wb-icon> </md-button> </div> </md-toolbar> <md-dialog-content> <form name=myForm layout=column layout-padding> <span flex=10></span> <md-input-container required> <label translate=\"\">Key</label> <input ng-model=config.data.key name=key disabled required> </md-input-container> <md-input-container> <label translate=\"\">Value</label> <input ng-model=config.data.value name=value required> <div ng-messages=myForm.value.$error ng-show=ctrl.showError> <div ng-message=required translate>This field is required.</div> </div> </md-input-container> <md-input-container> <label translate=\"\">Unit</label> <input ng-model=config.data.unit> </md-input-container> <md-input-container> <label translate=\"\">Namespace</label> <input ng-model=config.data.namespace> </md-input-container> </form> </md-dialog-content> </md-dialog>"
  );


  $templateCache.put('views/dialogs/product-new.html',
    "<md-dialog layout=column ng-cloak flex=50> <md-toolbar> <div class=md-toolbar-tools> <h2 translate=\"\">New product</h2> <span flex></span> <md-button class=md-icon-button ng-click=answer(config) ng-disabled=myForm.$invalid> <wb-icon aria-label=\"Close dialog\">done</wb-icon> </md-button> <md-button class=md-icon-button ng-click=cancel()> <wb-icon aria-label=\"Close dialog\">close</wb-icon> </md-button> </div> </md-toolbar> <md-dialog-content layout-padding> <form name=myForm ng-action=add(config) layout=column flex> <md-input-container> <label translate=\"\">Name</label> <input ng-model=config.title name=title required> <div ng-messages=myForm.title.$error> <div ng-message=required translate=\"\">This field is required.</div> </div> </md-input-container> <md-input-container> <label translate=\"\">Description</label> <input ng-model=config.description> </md-input-container> <md-input-container> <label tranlate>Price</label> <input ng-model=config.price type=number required> <div ng-messages=myForm.title.$error> <div ng-message=required translate=\"\">This field is required.</div> </div> </md-input-container> <md-input-container> <label tranlate=\"\">Off</label> <input ng-model=config.off> </md-input-container> <md-input-container> <label translate=\"\">Manufacturer</label> <input ng-model=config.manufacturer> </md-input-container> <md-input-container> <label translate=\"\">Brand</label> <input ng-model=config.brand> </md-input-container> <md-input-container> <label translate=\"\">Model</label> <input ng-model=config.model> </md-input-container> <wb-ui-setting-image title=\"{{'Image' | translate}}\" wb-ui-setting-clear-button=true wb-ui-setting-preview=true ng-model=config.avatar> </wb-ui-setting-image> </form> </md-dialog-content> </md-dialog>"
  );


  $templateCache.put('views/dialogs/select-categories.html',
    "<md-dialog layout=column ng-cloak> <md-toolbar> <div class=md-toolbar-tools> <h2 translate=\"\">Select categories</h2> <span flex></span> <md-button class=md-icon-button ng-click=answer(config.data) ng-disabled=myForm.$invalid> <wb-icon aria-label=\"Close dialog\">done</wb-icon> </md-button> <md-button class=md-icon-button ng-click=cancel()> <wb-icon aria-label=\"Close dialog\">close</wb-icon> </md-button> </div> </md-toolbar> <md-dialog-content layout-padding> <div layout=column> <md-checkbox ng-repeat=\"category in config.data track by $index\" ng-checked=category.selected ng-click=\"category.selected = !category.selected\"> {{category.name}} </md-checkbox> </div> </md-dialog-content> </md-dialog>"
  );


  $templateCache.put('views/dialogs/select-file.html',
    "<md-dialog aria-label=\"Select File\" ng-cloak> <form flex layout=column ng-action=answer(config)> <md-toolbar> <div class=md-toolbar-tools> <h2 translate=\"\">Select File</h2> <span flex></span> <md-button class=md-icon-button ng-click=answer(config)> <wb-icon aria-label=close>done</wb-icon> </md-button> <md-button class=md-icon-button ng-click=cancel()> <wb-icon aria-label=close>close</wb-icon> </md-button> </div> </md-toolbar> <md-dialog-content layout=column layout-padding> <md-input-container> <lf-ng-md-file-input lf-files=config.files progress preview drag></lf-ng-md-file-input> </md-input-container> </md-dialog-content> </form> </md-dialog>"
  );


  $templateCache.put('views/dialogs/show-image.html',
    "<md-dialog flex=50 layout=column ng-cloak layout-align=\"center center\"> <md-toolbar> <div class=md-toolbar-tools> <h2 ng-if=\"config.attachment.mime_type.includes('image')\" translate=\"\"> {{config.attachment.description}} </h2> <h2 ng-if=\"!config.attachment.mime_type.includes('image')\" translate=\"\"> Unknown type </h2> <span flex></span> <md-button class=md-icon-button ng-click=cancel()> <wb-icon aria-label=\"Close dialog\">close</wb-icon> </md-button> </div> </md-toolbar> <md-dialog-content layout-padding> <img ng-if=\"config.attachment.mime_type.includes('image')\" ng-src=/api/v2/shop/orders/{{config.order.id}}/attachments/{{config.attachment.id}}/content ng-src-error=\"src/resources/images/baseline-help-24px.svg\"> <img ng-if=\"!config.attachment.mime_type.includes('image')\" ng-src=\"src/resources/images/unknown-file.png\"> </md-dialog-content> </md-dialog>"
  );


  $templateCache.put('views/dialogs/tag-select.html',
    "<md-dialog ng-controller=TagsCtrl aria-label=Tag> <md-toolbar> <div class=md-toolbar-tools> <h2 translate=\"\">Select tag</h2> <span flex></span> <md-button class=md-icon-button ng-click=answer(ctrl.selected)> <wb-icon aria-label=select>done</wb-icon> </md-button> <md-button class=md-icon-button ng-click=cancel()> <wb-icon aria-label=\"Close dialog\">close</wb-icon> </md-button> </div> </md-toolbar> <md-dialog-content ng-init=nextPage() amd-infinate-scroll=nextPage() layout=column layout-padding flex> <md-list flex> <md-list-item class=md-3-line ng-click=\"ctrl.selected = tag\" ng-class=\"{true:'md - raised md - primary', false:''}[ctrl.selected.id === tag.id]\" ng-style=\"{'background-color': ctrl.selected.id === tag.id ? '#cce2ff' : ''}\" ng-repeat=\"tag in ctrl.items\"> <div class=md-list-item-text layout=column> <h4 translate=\"\">ID: {{tag.id}}</h4> <h3 translate=\"\">Name: {{tag.name}}</h3> <p>{{'Description' | translate}}: {{tag.description}}</p> </div> <md-divider></md-divider> </md-list-item>  <div layout=column layout-align=\"center center\"> <md-progress-circular ng-show=\"ctrl.status === 'working'\" md-diameter=96> {{'Loading...' | translate}} </md-progress-circular> </div> </md-list> </md-dialog-content> <md-dialog-actions layout=row> <span flex></span> <form ng-submit=search(temp.query)> <md-input-container class=\"md-icon-float md-icon-right md-block\"> <label translate=\"\">Search</label> <wb-icon>search</wb-icon> <input ng-model=temp.query> </md-input-container> <input type=submit hide> </form> </md-dialog-actions> </md-dialog>"
  );


  $templateCache.put('views/resources/amd-shop-categories.html',
    "<div ng-controller=\"MbSeenShopCategoriesCtrl as ctrl\" mb-preloading=\"ctrl.state === 'busy'\" layout=column flex> <mb-pagination-bar mb-model=ctrl.queryParameter mb-properties=ctrl.properties mb-reload=ctrl.reload() mb-more-actions=ctrl.getActions()> </mb-pagination-bar> <md-content mb-infinate-scroll=ctrl.loadNextPage() layout=column flex> <md-list flex> <md-list-item ng-repeat=\"role in ctrl.items track by role.id\" ng-click=\"multi || resourceCtrl.setSelected(role)\" class=md-3-line> <wb-icon>accessibility</wb-icon> <div class=md-list-item-text layout=column> <h3>{{role.name}}</h3> <p>{{role.description}}</p> </div> <md-checkbox class=md-secondary ng-init=\"role.selected = resourceCtrl.isSelected(role)\" ng-model=role.selected ng-click=\"resourceCtrl.setSelected(role, role.selected)\"> </md-checkbox> <md-divider md-inset></md-divider> </md-list-item> </md-list> </md-content> </div>"
  );


  $templateCache.put('views/resources/amd-shop-zones.html',
    "<div ng-controller=\"MbSeenShopZonesCtrl as ctrl\" mb-preloading=\"ctrl.state === 'busy'\" layout=column flex> <mb-pagination-bar mb-model=ctrl.queryParameter mb-properties=ctrl.properties mb-reload=ctrl.reload() mb-more-actions=ctrl.getActions()> </mb-pagination-bar> <md-content mb-infinate-scroll=ctrl.loadNextPage() layout=column flex> <md-list flex> <md-list-item ng-repeat=\"item in ctrl.items track by item.id\" ng-click=\"multi || resourceCtrl.setSelected(item)\" class=md-3-line> <wb-icon>accessibility</wb-icon> <div class=md-list-item-text layout=column> <h3>{{item.name}}</h3> <p>{{item.description}}</p> </div> <md-checkbox ng-if=multi class=md-secondary ng-init=\"item.selected = resourceCtrl.isSelected(item)\" ng-model=item.selected ng-click=\"resourceCtrl.setSelected(item, item.selected)\"> </md-checkbox> <md-divider md-inset></md-divider> </md-list-item> </md-list> </md-content> </div>"
  );

}]);
